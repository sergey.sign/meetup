{-# OPTIONS_GHC -fno-warn-orphans #-}

{- Note [TimeZone Conversion]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It happens so that Haskell has few different types for handling time
zones. One is 'TimeZone' in 'time' package, another is 'TZ' from 'tz'
package. You can get former from latter by calling
'Data.Time.Zones.timeZoneForUTCTime :: TZ -> UTCTime -> TimeZone'. As
one can see, it requires you to provide a timestamp **when** you need
the timezone, since it might differ at a different point in time.

We make a shortcut by using the UTC version of time to get that zone,
even though in theory it might differ, it seems to be good enough to
work in all cases for us.
-}

{- Note [TimeZone and Wire]

We pass time in number of milliseconds over the wire that's already
zoned, so we parse it as UTCTime and then convert into ZonedTime
without the usual zone arithmetics. Use 'localTimeToUTCTZ' to further
get the 'TimeZone' object.
-}

module Data.Time.Additional where

import Control.Concurrent (threadDelay)
import Data.Hashable
import Data.Time
import Data.Time.Calendar.WeekDate
import qualified Data.Time.Clock as Time
import Data.Time.Clock.POSIX
import qualified Data.Time.Clock.POSIX as Time
import qualified Data.Time.Zones
import Prelude

-- | Get the seconds in a 'NominalDiffTime'.
--
-- @since 1.9.1
nominalDiffTimeToSeconds :: Time.NominalDiffTime -> Int
nominalDiffTimeToSeconds t = floor (toRational t)

nominalDiffTimeToMilliseconds :: Time.NominalDiffTime -> Int
nominalDiffTimeToMilliseconds t = (nominalDiffTimeToSeconds t) * 1000

millisecondsToNominalDiffTime :: Int -> Time.NominalDiffTime
millisecondsToNominalDiffTime t = fromIntegral (t `div` 1000)

utcTimeToMilliseconds :: Time.UTCTime -> Int
utcTimeToMilliseconds =
  nominalDiffTimeToMilliseconds . Time.utcTimeToPOSIXSeconds

millisecondsToUtcTime :: Int -> Time.UTCTime
millisecondsToUtcTime =
  Time.posixSecondsToUTCTime . millisecondsToNominalDiffTime

-- | See Note [TimeZone and Wire]
zonedTimeToMilliseconds :: ZonedTime -> Int
zonedTimeToMilliseconds (ZonedTime localTime _tz) =
  utcTimeToMilliseconds (localTimeToUTC utc localTime)

-- | If you got milliseconds that you know are zoned over the wire,
-- construct a zoned time
--
-- See Note [TimeZone Conversion]
millisecondsToZonedTime :: Data.Time.Zones.TZ -> Int -> ZonedTime
millisecondsToZonedTime tz' ms =
  let utctime = millisecondsToUtcTime ms
      tz'' = Data.Time.Zones.timeZoneForUTCTime tz' utctime
   in ZonedTime (utcToLocalTimeNoConv utctime) tz''

addZonedTime :: NominalDiffTime -> ZonedTime -> ZonedTime
addZonedTime d (ZonedTime localTime tz) =
  ZonedTime (addLocalTime d localTime) tz

-- | See Note [TimeZone and Wire]
utcToLocalTimeNoConv :: UTCTime -> LocalTime
utcToLocalTimeNoConv (UTCTime day dt) = LocalTime day (timeToTimeOfDay dt)

-- | See Note [TimeZone Conversion]
utcToZonedTime' :: Data.Time.Zones.TZ -> UTCTime -> ZonedTime
utcToZonedTime' tz t =
  utcToZonedTime (Data.Time.Zones.timeZoneForUTCTime tz t) t

-- | Format taken by threadDelay and friends
nominalDiffToMicroSeconds :: Integral a => NominalDiffTime -> a
nominalDiffToMicroSeconds x = truncate ((realToFrac x :: Double) * 1000000)

threadDelaySecs :: NominalDiffTime -> IO ()
threadDelaySecs n = threadDelay (nominalDiffToMicroSeconds n)

-- | Finds the immediately preceding 'UTCTime' value which represents the
-- beginning of a ISO 8601 Week (i.e., is a Monday) and has no fractional
-- daypart.
--
-- >>> rewindToWeek $ read "2015-06-30 23:59:60.123 UTC"
-- 2015-06-29 00:00:00 UTC
rewindToWeek :: UTCTime -> UTCTime
rewindToWeek (UTCTime day _) =
  let (year, week, _) = toWeekDate day
   in UTCTime (fromWeekDate year week 1) 0

-- | Assuming Monday here, might change in future
rewindDayToWeek :: Day -> Day
rewindDayToWeek day =
  let subtr =
        case dayOfWeek day of
          Monday -> 0
          Tuesday -> 1
          Wednesday -> 2
          Thursday -> 3
          Friday -> 4
          Saturday -> 6
          Sunday -> 7
   in addDays ((-1) * subtr) day

rewindToWeekLocal :: LocalTime -> LocalTime
rewindToWeekLocal (LocalTime day _timeOfDay) =
  LocalTime (rewindDayToWeek day) (TimeOfDay 0 0 0)

rewindToDayLocal :: LocalTime -> LocalTime
rewindToDayLocal (LocalTime day _timeOfDay) =
  LocalTime day (TimeOfDay 0 0 0)

rewindToDayZoned :: ZonedTime -> ZonedTime
rewindToDayZoned (ZonedTime localTime tz) =
  ZonedTime (rewindToDayLocal localTime) tz

rewindToWeekZoned :: ZonedTime -> ZonedTime
rewindToWeekZoned (ZonedTime localTime tz) =
  ZonedTime (rewindToWeekLocal localTime) tz

-- | Collisions occur between all fractional seconds and between leap seconds
-- and their succeeding seconds. Contrast with the performance of a 'show'
-- based hash.
instance Hashable UTCTime where
  hashWithSalt salt = hashWithSalt salt . toRational . utcTimeToPOSIXSeconds

instance Hashable ZonedTime where
  hashWithSalt salt = hashWithSalt salt . zonedTimeToUTC

instance Eq ZonedTime where
  z1 == z2 = zonedTimeToUTC z1 == zonedTimeToUTC z2

instance Hashable Day where
  hashWithSalt salt = hashWithSalt salt . toModifiedJulianDay

-- * Taken from time-1.9
data DayOfWeek
  = Monday
  | Tuesday
  | Wednesday
  | Thursday
  | Friday
  | Saturday
  | Sunday
  deriving (Eq, Show, Read)

-- | \"Circular\", so for example @[Tuesday ..]@ gives an endless sequence.
-- Also: 'fromEnum' gives [1 .. 7] for [Monday .. Sunday], and 'toEnum' performs mod 7 to give a cycle of days.
instance Enum DayOfWeek where
  toEnum i =
    case mod i 7 of
      0 -> Sunday
      1 -> Monday
      2 -> Tuesday
      3 -> Wednesday
      4 -> Thursday
      5 -> Friday
      _ -> Saturday
  fromEnum Monday = 1
  fromEnum Tuesday = 2
  fromEnum Wednesday = 3
  fromEnum Thursday = 4
  fromEnum Friday = 5
  fromEnum Saturday = 6
  fromEnum Sunday = 7
  enumFromTo wd1 wd2
    | wd1 == wd2 = [wd1]
  enumFromTo wd1 wd2 = wd1 : enumFromTo (succ wd1) wd2
  enumFromThenTo wd1 wd2 wd3
    | wd2 == wd3 = [wd1, wd2]
  enumFromThenTo wd1 wd2 wd3 =
    wd1 : enumFromThenTo wd2 (toEnum $ (2 * fromEnum wd2) - (fromEnum wd1)) wd3

dayOfWeek :: Day -> DayOfWeek
dayOfWeek (ModifiedJulianDay d) = toEnum $ fromInteger $ d + 3

-- | addLocalTime a b = a + b
addLocalTime :: NominalDiffTime -> LocalTime -> LocalTime
addLocalTime x = utcToLocalTime utc . addUTCTime x . localTimeToUTC utc

deriving instance Ord ZonedTime
