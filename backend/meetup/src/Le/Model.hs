{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Le.Model where

import Data.Time.Clock (UTCTime)
import Database.Persist.Postgresql
import Database.Persist.TH
import Le.Import
import Le.Types

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  [persistLowerCase|
User
    email Text
    phoneNumber PhoneNumber Maybe
    fullName FullName Maybe
    avatar_300 PersistentAbsoluteURI Maybe
    avatarOrig PersistentAbsoluteURI Maybe
    admin Bool
    createdAt UTCTime
    updatedAt UTCTime
    UniqueEmail email
    deriving Show
LoginToken
    tokenVal LoginTokenVal
    userId UserId
    createdAt UTCTime
    LoginTokenQuery tokenVal
    Primary tokenVal
    deriving Show
LoginCode
    code Text
    email Text
    createdAt UTCTime
    Primary code
    deriving Show
Meetup
    title Text
    description Text
    organizerId UserId
    memberApprovalRequired Bool Maybe
    timeZone TZLabel Maybe
    deriving Show
Event
    meetupId MeetupId
    date UTCTime
    location Text Maybe
    locationLink PersistentAbsoluteURI Maybe
    deriving Show
Rsvp
    userId UserId
    eventId EventId
    deriving Show
ApplicationField
    formId ApplicationFormId
    description Text
    deriving Show
ApplicationForm
    meetupId MeetupId
    description Text
    deriving Show
UserApplicationField
    fieldId ApplicationFieldId
    userId UserId
    content Text
    deriving Show
UserApplication
    formId ApplicationFormId
    userId UserId
    deriving Show
ApprovedMember
    meetupId MeetupId
    userId UserId
    status ApprovalStatus
    deriving Show
FeedbackForm
    meetupId MeetupId
    allowFillSince UTCTime
    title Text
    description Text
    deriving Show
Feedback
    feedbackFormId FeedbackFormId
    userId UserId
    content Text
    complete Bool
    deriving Show
|]
