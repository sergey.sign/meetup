{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Le.GenerateElm where

import qualified Data.String.Class as S
import qualified Data.Text as T
import Elm.Module
import Elm.TyRep (EType)
import Elm.Versions
import qualified Le.ApiTypes as AT
import Le.Routes
import Le.Types
import RIO
import Servant
import Servant.API.Generic
import Servant.Elm
import Servant.Elm.Internal.Foreign
import Servant.Foreign
import qualified Text.InterpolatedString.Perl6 as P6

instance HasForeign LangElm EType a =>
         HasForeign LangElm EType (AuthProtect "cookie-auth" :> a) where
  type Foreign EType (AuthProtect "cookie-auth" :> a) = Foreign EType a
  foreignFor l ft _api req = foreignFor l ft (Proxy :: Proxy a) req

moduleDefs :: [DefineElm]
moduleDefs =
  [ DefineElm (Proxy :: Proxy AT.JsonURI)
  , DefineElm (Proxy :: Proxy Condition)
  , DefineElm (Proxy :: Proxy AT.Meetup)
  , DefineElm (Proxy :: Proxy AT.UserMeetupInfo)
  , DefineElm (Proxy :: Proxy AT.MeetupAuthInfo)
  , DefineElm (Proxy :: Proxy AT.Event)
  , DefineElm (Proxy :: Proxy AT.AccountInfo)
  , DefineElm (Proxy :: Proxy AT.PublicAccountInfo)
  , DefineElm (Proxy :: Proxy AT.LogInSendPasswordForm)
  , DefineElm (Proxy :: Proxy AT.LogInSendCodeForm)
  , DefineElm (Proxy :: Proxy AT.UpdateAccountForm)
  , DefineElm (Proxy :: Proxy AT.RsvpInfo)
  , DefineElm (Proxy :: Proxy AT.AccountAttendedMeetup)
  , DefineElm (Proxy :: Proxy AT.FeedbackForm)
  , DefineElm (Proxy :: Proxy AT.Feedback)
  , DefineElm (Proxy :: Proxy AT.MeetupUpdateForm)
  , DefineElm (Proxy :: Proxy AT.UpdateFeedback)
  , DefineElm (Proxy :: Proxy AT.ApplicationField)
  , DefineElm (Proxy :: Proxy AT.ApplicationForm)
  , DefineElm (Proxy :: Proxy AT.ApplicationFieldUpdateForm)
  , DefineElm (Proxy :: Proxy AT.UserApplication)
  , DefineElm (Proxy :: Proxy AT.UserApplicationField)
  , DefineElm (Proxy :: Proxy AT.EventUpdateForm)
  ]

replacements :: [(Text, Text)]
replacements =
  [ ("(Key User)", "UserId")
  , ("(Key Meetup)", "MeetupId")
  , ("(Key ApplicationField)", "ApplicationFieldId")
  , ("(Key ApplicationForm)", "ApplicationFormId")
  , ("(Key Event)", "EventId")
  , ("(Key PhysicalBook)", "PhysicalBookId")
  , ("(Key Location)", "LocationId")
  , ("(Key PendingPickup)", "PendingPickupId")
  , ("(Key WeeklyPaymentSubscription)", "WeeklyPaymentSubscriptionId")
  , ("(Key WeeklyPaymentSingle)", "WeeklyPaymentSingleId")
  , ("(Key FeedbackForm)", "FeedbackFormId")
  , ("(Key UserApplicationField)", "UserApplicationFieldId")
  , ("(Key UserApplication)", "UserApplicationId")
  , ("Http.expectJson", "leExpectJson")
  , ("Http.Error", "Error")
  , ( "Err e -> toMsg (Err e)"
    , "Err e -> toMsg (Err {httpError=e,formErrors=Dict.empty,errors=[]})")
  ]

intAliases :: [String]
intAliases =
  mkIntAliases
    [ "IntUTCTime"
    , "IntZonedTime"
    , "Int64"
    , "MeetupId"
    , "EventId"
    , "FeedbackFormId"
    , "UserId"
    , "ApplicationFieldId"
    , "ApplicationFormId"
    , "ApprovedMemberId"
    , "UserApplicationId"
    , "UserApplicationFieldId"
    ]

stringAliases :: [String]
stringAliases = mkStringAliases []

elmHeader :: String
elmHeader =
  [P6.q|module Le.Api exposing (..)

import Http
import Json.Decode exposing (Value)
import Json.Encode
import Json.Helpers exposing (required, fnullable, maybeEncode, decodeSumUnaries)
import Dict exposing (Dict)
import Url.Builder

type alias Text = String
jsonDecText = Json.Decode.string

type alias Tuple2 a b = { t2f1 : a, t2f2 : b }

jsonDecTuple2 : Json.Decode.Decoder dec1 -> Json.Decode.Decoder dec2 -> Json.Decode.Decoder (Tuple2 dec1 dec2)
jsonDecTuple2 dec1 dec2 =
   Json.Decode.succeed (\pv1 pv2 -> {t2f1 = pv1, t2f2 = pv2})
   |> required "t2f1" dec1
   |> required "t2f2" dec2

jsonPair : Json.Decode.Decoder a -> Json.Decode.Decoder b -> Json.Decode.Decoder ( a, b )
jsonPair a b =
    Json.Decode.andThen
        (\xs ->
            case xs of
                [] ->
                    Json.Decode.fail "Expecting a list of two elements"

                x :: y :: [] ->
                    case ( Json.Decode.decodeValue a x, Json.Decode.decodeValue b y ) of
                        ( Ok av, Ok bv ) ->
                            Json.Decode.succeed ( av, bv )

                        _ ->
                            Json.Decode.fail "Error while decoding individual pair fields"

                _ ->
                    Json.Decode.fail "Expecting a list of two elements"
        )
        (Json.Decode.list Json.Decode.value)

type alias Error =
    { httpError : Http.Error
    , formErrors : Dict String String
    , errors : List String
    }

leExpectJson : (Result Error a -> msg) -> Json.Decode.Decoder a -> Http.Expect msg
leExpectJson toMsg decoder =
    Http.expectStringResponse toMsg <|
        \response ->
            case response of
                Http.BadUrl_ url ->
                    Err { httpError = Http.BadUrl url, formErrors = Dict.empty, errors = [] }

                Http.Timeout_ ->
                    Err { httpError = Http.Timeout, formErrors = Dict.empty, errors = [] }

                Http.NetworkError_ ->
                    Err { httpError = Http.NetworkError, formErrors = Dict.empty, errors = [] }

                Http.BadStatus_ metadata body ->
                    case metadata.statusCode of
                        400 ->
                            case Json.Decode.decodeString (Json.Decode.list (jsonPair Json.Decode.string Json.Decode.string)) body of
                                Err decodeErr ->
                                    Err
                                        { httpError = Http.BadStatus metadata.statusCode
                                        , errors = [ "Error while decoding a back-end response: " ++ Json.Decode.errorToString decodeErr ]
                                        , formErrors = Dict.empty
                                        }

                                Ok vs ->
                                    Err
                                        { httpError = Http.BadStatus metadata.statusCode
                                        , errors = []
                                        , formErrors = Dict.fromList vs
                                        }

                        _ ->
                            Err { httpError = Http.BadStatus metadata.statusCode, formErrors = Dict.empty, errors = ["There was a back-end error. Try again or contact administrators"] }

                Http.GoodStatus_ metadata body ->
                    case Json.Decode.decodeString decoder body of
                        Ok value ->
                            Ok value

                        Err err ->
                            Err { httpError = Http.BadBody (Json.Decode.errorToString err), formErrors = Dict.empty, errors = [] }
|]

generateElmNew :: IO ()
generateElmNew = do
  S.putStrLn elmHeader
  let defs = makeModuleContent moduleDefs
  S.putStrLn defs
  forM_ intAliases $ \t -> do S.putStrLn t
  forM_ stringAliases $ \t -> do S.putStrLn t
  forM_ (generateElmForAPI (Proxy :: Proxy (ToServantApi JsonAPI))) $ \t -> do
    S.putStrLn (doReplaces t)
    S.putStrLn ("" :: Text)

doReplaces :: Text -> Text
doReplaces s =
  foldl' (\curr (from, to') -> T.replace from to' curr) s replacements

generateElm :: IO ()
generateElm = do
  S.putStrLn $
    (unlines $
     [ moduleHeader Elm0p18 "Le.Api"
     , ""
     , "import Json.Decode"
     , "import Json.Encode exposing (Value)"
     , "-- The following module comes from bartavelle/json-helpers"
     , "import Json.Helpers exposing (..)"
     , "import Dict exposing (Dict)"
     , "import Set exposing (Set)"
     , ""
     ] ++
     intAliases ++ [""] ++ stringAliases ++ [""]) ++
    (makeModuleContent moduleDefs)

mkIntAlias :: String -> [String]
mkIntAlias x =
  [ "type alias " <> x <> " = Int"
  , "jsonDec" <> x <> " = Json.Decode.int"
  , "jsonEnc" <> x <> " = Json.Encode.int"
  ]

mkIntAliases :: Foldable t => t String -> [String]
mkIntAliases xs = concatMap mkIntAlias xs

mkStringAlias :: String -> [String]
mkStringAlias x =
  [ "type alias " <> x <> " = String"
  , "jsonDec" <> x <> " = Json.Decode.string"
  , "jsonEnc" <> x <> " = Json.Encode.string"
  ]

mkStringAliases :: Foldable t => t String -> [String]
mkStringAliases xs = concatMap mkStringAlias xs
