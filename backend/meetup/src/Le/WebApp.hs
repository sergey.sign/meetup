{-# LANGUAGE TypeOperators #-}

module Le.WebApp where

import qualified Control.Concurrent.Async as Async
import Control.Monad.Trans.Except (ExceptT(..))
import qualified Data.ByteString.Char8 as BC8
import Data.Maybe (fromMaybe)
import Data.Pool (Pool)
import Data.Proxy
import qualified Data.String.Class as S
import qualified Database.Persist.Postgresql as P
import GHC.Stack
import qualified Le.GenerateElm as GenerateElm
import Le.Handlers
import Le.Model
import Le.Routes
import Le.StubData
import Le.Types
import qualified Network.HTTP.Client.TLS as HTTPClient
import qualified Network.HTTP.Types.Status as HTTPStatus
import Network.Wai (Request)
import qualified Network.Wai as Wai
import qualified Network.Wai.Handler.Warp as Warp
import qualified Network.Wai.Handler.WarpTLS as WarpTLS
import qualified Network.Wai.Middleware.Gzip as Gzip
import qualified Network.Wai.Middleware.Static as MStatic
import RIO
import Safe (readMay)
import Servant
import Servant.API.Generic (ToServantApi, genericApi)
import Servant.Server.Experimental.Auth (AuthHandler)
import Servant.Server.Generic (genericServerT)
import System.Environment (getArgs, lookupEnv)

defaultHost :: ByteString
defaultHost = "meetup.kyiv.ua"

api :: Proxy (ToServantApi API)
api = genericApi (Proxy :: Proxy API)

-- | Redirects to https
httpApp :: Int -> Application
httpApp httpsPort req respond = respond response
  where
    mHostAndPort = Wai.requestHeaderHost req
    hostAndPort = fromMaybe defaultHost mHostAndPort
    host =
      case BC8.split ':' hostAndPort of
        (h:_) -> h
        _ -> defaultHost
    hostWithProtocol =
      "https://" <> host <> ":" <> S.fromString (show httpsPort)
    response =
      Wai.responseLBS HTTPStatus.status301 [("Location", hostWithProtocol)] ""

nt :: Env -> AppM a -> Servant.Handler a
nt s x = Servant.Handler $ ExceptT $ try $ runReaderT x s

withPostgresqlPool ::
     P.ConnectionString
  -> Int
  -> (Pool P.SqlBackend -> RIO RioApp a)
  -> RIO RioApp a
withPostgresqlPool = P.withPostgresqlPool

genAuthServerContext ::
     Pool P.SqlBackend -> Context (AuthHandler Request (P.Entity User) ': '[])
genAuthServerContext db = (authHandler db) :. EmptyContext

onExceptionAct :: HasCallStack => Maybe Wai.Request -> SomeException -> IO ()
onExceptionAct mReq exc = do
  let url =
        case mReq of
          Nothing -> ""
          Just req -> S.toText (Wai.rawPathInfo req)
  S.putStrLn
    ("Error: caught an exception Path: " <> url <> " Error: " <> tshow exc <>
     ". Call stack: " <>
     S.toText (prettyCallStack callStack))

main :: IO ()
main = do
  let psqlConnString =
        "host=localhost port=5432 user=postgres dbname=meetup password=password"
  let isVerbose = False
  logOptions' <- logOptionsHandle stderr isVerbose
  let logOptions = setLogUseTime True logOptions'
  httpManager <- HTTPClient.newTlsManager
  withLogFunc logOptions $ \lf -> do
    let app = RioApp {appLogFunc = lf}
    runRIO app $ do
      args <- liftIO getArgs
      case args of
        ["generate-elm"] -> do
          liftIO GenerateElm.generateElmNew
        ["generate-elm-old"] -> do
          liftIO GenerateElm.generateElm
        ["migrate"] -> do
          P.withPostgresqlPool psqlConnString 1 $ \pool -> do
            liftIO $ flip P.runSqlPool pool $ P.runMigration migrateAll
        ["drop-db-data"] -> do
          withPostgresqlPool psqlConnString 1 $ \pool -> dropDbData pool
        ["insert-stub-data"] -> do
          withPostgresqlPool psqlConnString 1 $ \pool -> insertStubData pool
        _ -> do
          logInfo "Starting app" :: RIO RioApp ()
          httpsPort <-
            return . fromMaybe 8081 . (>>= readMay) =<<
            liftIO (lookupEnv "MEETUP_HTTPS_PORT")
          httpPort <-
            return . fromMaybe 8080 . (>>= readMay) =<<
            liftIO (lookupEnv "MEETUP_HTTP_PORT")
          certPath <-
            return . fromMaybe "sysadmin/keys/certificate.pem" =<<
            liftIO (lookupEnv "MEETUP_CERT")
          keyPath <-
            return . fromMaybe "sysadmin/keys/key.pem" =<<
            liftIO (lookupEnv "MEETUP_KEY")
          mailgunDomain <-
            return . fromMaybe "meetup.events" =<<
            liftIO (lookupEnv "MEETUP_MAILGUN_DOMAIN")
          mailgunApiKey <-
            return . fromMaybe "" =<<
            liftIO (lookupEnv "MEETUP_MAILGUN_API_KEY")
          when (mailgunApiKey == "") $ do
            logError "Mailgun API key is empty!"
            error "Mailgun API key is empty!"
          facebookAppId <-
            return . fromMaybe "" =<<
            liftIO (lookupEnv "MEETUP_FACEBOOK_APP_ID")
          facebookAppSecret <-
            return . fromMaybe "" =<<
            liftIO (lookupEnv "MEETUP_FACEBOOK_APP_SECRET")
          facebookAppToken <-
            return . fromMaybe "" =<<
            liftIO (lookupEnv "MEETUP_FACEBOOK_APP_TOKEN")
          googleOauthClientId <-
            return . fromMaybe "" =<<
            liftIO (lookupEnv "MEETUP_GOOGLE_OAUTH_CLIENT_ID")
          googleOauthClientSecret <-
            return . fromMaybe "" =<<
            liftIO (lookupEnv "MEETUP_GOOGLE_OAUTH_CLIENT_SECRET")
          P.withPostgresqlPool psqlConnString 5 $ \pool -> do
            let env =
                  Env
                    { envDb = pool
                    , envLogFunc = lf
                    , envConfig =
                        EnvConfig
                          { cfgMailgunDomain = S.toText mailgunDomain
                          , cfgMailgunApiKey = S.toText mailgunApiKey
                          , cfgFacebookAppId = S.toText facebookAppId
                          , cfgFacebookAppSecret = S.toText facebookAppSecret
                          , cfgFacebookAppToken = S.toText facebookAppToken
                          , cfgGoogleOauthClientId =
                              S.toText googleOauthClientId
                          , cfgGoogleOauthClientSecret =
                              S.toText googleOauthClientSecret
                          , cfgCertPath = S.toText certPath
                          , cfgKeyPath = S.toText keyPath
                          , cfgHttpPort = httpPort
                          , cfgHttpsPort = httpsPort
                          }
                    , envHttpManager = httpManager
                    }
            let tlsSettings = WarpTLS.tlsSettings certPath keyPath
            let settingsHttps =
                  Warp.setOnException onExceptionAct $
                  Warp.setPort httpsPort Warp.defaultSettings
            let cfg = envConfig env
            let runHttp :: RIO RioApp ()
                runHttp =
                  liftIO
                    (Warp.run (cfgHttpPort cfg) (httpApp (cfgHttpsPort cfg)))
            cacheContainer <-
              liftIO $ MStatic.initCaching MStatic.PublicStaticCaching
            let waiApp :: Wai.Application
                waiApp =
                  Gzip.gzip
                    (Gzip.def {Gzip.gzipFiles = Gzip.GzipCompress})
                    (MStatic.static'
                       cacheContainer
                       (serveWithContext
                          (Proxy :: Proxy (ToServantApi API))
                          (genAuthServerContext pool)
                          (hoistServerWithContext
                             (Proxy :: Proxy (ToServantApi API))
                             (Proxy :: Proxy '[ AuthHandler Wai.Request (P.Entity User)])
                             (nt env)
                             (genericServerT server))))
            let runHttps :: RIO RioApp ()
                runHttps =
                  liftIO (WarpTLS.runTLS tlsSettings settingsHttps waiApp)
            withRunInIO $ \runInIO ->
              (Async.concurrently_ (runInIO runHttp) (runInIO runHttps))

mkIntAlias :: String -> [String]
mkIntAlias x =
  [ "type alias " <> x <> " = Int"
  , "jsonDec" <> x <> " = Json.Decode.int"
  , "jsonEnc" <> x <> " = Json.Encode.int"
  ]

mkIntAliases :: Foldable t => t String -> [String]
mkIntAliases xs = concatMap mkIntAlias xs
