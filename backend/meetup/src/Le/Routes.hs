{-# LANGUAGE TypeOperators #-}

-- | This file should not be re-indented with hindent
module Le.Routes where

import Data.Text (Text)
import qualified Le.ApiTypes as AT
import Le.Handlers
import Le.Handlers.Social
import Le.Model
import Le.Types
import Le.Import
import Servant
import Servant.API.Generic
import Servant.Server.Generic
import Web.Cookie (SetCookie)
import qualified Data.String.Class as S
import Network.HTTP.Media ((//), (/:))

data HTML

instance Accept HTML where
  contentType _ = "text" // "html" /: ("charset", "utf-8")

instance S.ConvLazyByteString a => MimeRender HTML a where
   mimeRender _ val = S.toLazyByteString val

data API route = API
  { __index :: route :- Get '[HTML] Text
  , __login :: route :- "login" :> Get '[HTML] Text
  , __meetups :: route :-
      "meetups"
      :> Get '[HTML] Text
  , __meetup :: route :-
      "meetup"
      :> Capture "meetup-id" MeetupId
      :> Get '[HTML] Text
  , __meetupApplicationForm :: route :-
      "meetup"
      :> Capture "meetup-id" MeetupId
      :> "application-form"
      :> Get '[HTML] Text
  , __account :: route :-
      "account"
      :> Get '[HTML] Text
  , __accountMeetups :: route :-
      "account" :> "meetups"
      :> Get '[HTML] Text
  , __accountMeetupInfo :: route :-
      "account" :> "meetups"
      :> Capture "meetup-id" MeetupId
      :> "info"
      :> Get '[HTML] Text
  , __accountMeetupEvents :: route :-
      "account"
      :> "meetups"
      :> Capture "meetup-id" MeetupId
      :> "events"
      :> Get '[HTML] Text
  , __accountMeetupCreate :: route :-
      "account"
      :> "meetups"
      :> "create"
      :> Get '[HTML] Text
  , __accountMeetupEdit :: route :-
      "account"
      :> "meetups"
      :> Capture "meetup-id" MeetupId
      :> "edit"
      :> Get '[HTML] Text
  , __accountEventsCreate :: route :-
      "account"
      :> "meetups"
      :> Capture "meetup-id" MeetupId
      :> "events"
      :> "create"
      :> Get '[HTML] Text
  , __accountEventsEdit :: route :-
      "account"
      :> "meetups"
      :> Capture "meetup-id" MeetupId
      :> "events"
      :> Capture "event-id" EventId
      :> "edit"
      :> Get '[HTML] Text
  , __ping :: route :- "api" :> "ping" :> Get '[ PlainText] Text
  , __jsonApi :: route :- ToServantApi JsonAPI
  , __fbLoginCallback :: route :-
    "api" :> "fb-login-callback"
          :> QueryParam "error_code" Text
          :> QueryParam "error_message" Text
          :> QueryParam "code" Text
          :> QueryParam "state" Text
          :> Get '[HTML] (Headers '[ Header "Set-Cookie" SetCookie] Text)
  , __googleLoginCallback :: route :-
    "api" :> "google-login-callback"
          :> QueryParam "state" Text
          :> QueryParam "code" Text
          :> QueryParam "scope" Text
          :> QueryParam "error" Text
          :> Get '[HTML] (Headers '[ Header "Set-Cookie" SetCookie] Text)
  } deriving (Generic)

data JsonAPI route = JsonAPI
  { _meetupsList :: route :-
    "api" :> "meetups.json"
          :> Get '[ JSON] [AT.Meetup]
  , _meetupInfo :: route :-
    "api" :> "meetup" :> Capture "meetup-id" MeetupId :> "info.json"
          :> Get '[ JSON] AT.Meetup
  , _meetupAuthInfo :: route :- AuthProtect "cookie-auth" :>
    "api" :> "meetup" :> Capture "meetup-id" MeetupId :> "auth-info.json"
          :> Get '[ JSON] AT.MeetupAuthInfo
  , _eventsList :: route :-
    "api" :> "meetup" :> Capture "meetup-id" MeetupId :> "events.json"
          :> Get '[ JSON] [AT.Event]
  , _eventInfo :: route :-
    "api" :> "events" :> Capture "event-id" EventId :> "info.json"
          :> Get '[ JSON] AT.Event
  , _accountInfo :: route :- AuthProtect "cookie-auth" :>
    "api" :> "account-info.json"
          :> Get '[ JSON] AT.AccountInfo
  , _publicAccountInfo :: route :- AuthProtect "cookie-auth" :>
    "api" :> "user" :> Capture "user-id" UserId
          :> "public-account-info.json"
          :> Get '[ JSON] AT.PublicAccountInfo
  , _logInSendPassword :: route :-
    "api" :> "log-in-send-password"
          :> ReqBody '[ JSON] AT.LogInSendPasswordForm
          :> Post '[ JSON] ()
  , _logInSendCode :: route :-
    "api" :> "log-in"
          :> ReqBody '[ JSON] AT.LogInSendCodeForm
          :> Post '[ JSON] (Headers '[ Header "Set-Cookie" SetCookie] AT.AccountInfo)
  , _logOut :: route :-
    "api" :> "log-out"
          :> Get '[ JSON] (Headers '[ Header "Set-Cookie" SetCookie] ())
  , _updateAccount :: route :- AuthProtect "cookie-auth" :>
    "api" :> "account"
          :> "update.json"
          :> ReqBody '[ JSON] AT.UpdateAccountForm
          :> Post '[ JSON] AT.AccountInfo
  , _attendedMeetups :: route :- AuthProtect "cookie-auth" :>
    "api" :> "meetups"
          :> "attended.json"
          :> Get '[ JSON] [AT.AccountAttendedMeetup]
  , _meetupFeedbackForms :: route :- AuthProtect "cookie-auth" :>
    "api" :> "meetups"
          :> Capture "meetup-id" MeetupId
          :> "feedback-forms.json"
          :> Get '[ JSON] [AT.FeedbackForm]
  , _feedbackFormInfo :: route :- AuthProtect "cookie-auth" :>
    "api" :> "feedback-forms"
          :> Capture "feedback-form-id" FeedbackFormId
          :> "info.json"
          :> Get '[ JSON] AT.FeedbackForm
  , _feedbackGet :: route :- AuthProtect "cookie-auth" :>
    "api" :> "feedback-forms"
          :> Capture "feedback-form-id" FeedbackFormId
          :> "feedback.json"
          :> Get '[ JSON] AT.Feedback
  , _feedbackSet :: route :- AuthProtect "cookie-auth" :>
    "api" :> "feedback-forms"
          :> Capture "feedback-form-id" FeedbackFormId
          :> "set-feedback.json"
          :> ReqBody '[ JSON] AT.UpdateFeedback
          :> Post '[ JSON] AT.Feedback
  , _rsvpList :: route :-
    "api" :> "event" :> Capture "event-id" EventId :> "rsvp" :> "list.json"
          :> Get '[ JSON] [AT.RsvpInfo]
  , _rsvpCreate :: route :- AuthProtect "cookie-auth" :>
    "api" :> "event" :> Capture "event-id" EventId :> "rsvp" :> "create.json"
          :> Post '[ JSON] AT.RsvpInfo
  , _rsvpDelete :: route :- AuthProtect "cookie-auth" :>
    "api" :> "event" :> Capture "event-id" EventId :> "rsvp" :> "delete.json"
          :> Delete '[ JSON] ()
  , _applicationForm :: route :- AuthProtect "cookie-auth" :>
    "api" :> "meetup"
          :> Capture "meetup-id" MeetupId
          :> "application-form.json"
          :> Get '[ JSON] AT.ApplicationForm
  , _applicationFieldSave :: route :- AuthProtect "cookie-auth" :>
    "api" :> "application-field"
          :> Capture "application-field-id" ApplicationFieldId
          :> "update.json"
          :> ReqBody '[ JSON] AT.ApplicationFieldUpdateForm
          :> Post '[JSON] ()
  , _applicationFormSubmit :: route :- AuthProtect "cookie-auth" :>
    "api" :> "application-form"
          :> Capture "application-form-id" ApplicationFormId
          :> "submit.json"
          :> Post '[JSON] ()
  , _applicationFormDelete :: route :- AuthProtect "cookie-auth" :>
    "api" :> "meetup"
          :> Capture "meetup-id" MeetupId
          :> "application-form"
          :> "delete.json"
          :> Post '[JSON] ()
  , _userApplicationField :: route :- AuthProtect "cookie-auth" :>
    "api" :> "user-application-field"
          :> Capture "user-application-field-id" ApplicationFieldId
          :> "info.json"
          :> Get '[JSON] (Maybe AT.UserApplicationField)
  , _userMeetups :: route :- AuthProtect "cookie-auth" :>
    "api" :> "user" :> "meetups.json"
          :> Get '[JSON] [AT.Meetup]
  , _userMeetupInfo :: route :- AuthProtect "cookie-auth" :>
    "api" :> "user" :> "meetups"
          :> Capture "meetup-id" MeetupId
          :> "info.json"
          :> Get '[JSON] AT.UserMeetupInfo
  , _userMeetupCreate :: route :- AuthProtect "cookie-auth" :>
    "api" :> "user" :> "meetups"
          :> "create.json"
          :> ReqBody '[ JSON] AT.MeetupUpdateForm
          :> Post '[JSON] AT.UserMeetupInfo
  , _userMeetupUpdate :: route :- AuthProtect "cookie-auth" :>
    "api" :> "user" :> "meetups"
          :> Capture "meetup-id" MeetupId
          :> "update.json"
          :> ReqBody '[ JSON] AT.MeetupUpdateForm
          :> Post '[JSON] AT.UserMeetupInfo
  , _userMeetupApplications :: route :- AuthProtect "cookie-auth" :>
    "api" :> "user" :> "meetups"
          :> Capture "meetup-id" MeetupId
          :> "applications.json"
          :> Get '[JSON] [AT.UserApplication]
  , _userMeetupApplicationAccept :: route :- AuthProtect "cookie-auth" :>
    "api" :> "user" :> "meetups"
          :> "application"
          :> Capture "user-application-id" UserApplicationId
          :> "accept.json"
          :> Post '[JSON] ()
  , _userMeetupApplicationReject :: route :- AuthProtect "cookie-auth" :>
    "api" :> "user" :> "meetups"
          :> "application"
          :> Capture "user-application-id" UserApplicationId
          :> "reject.json"
          :> Post '[JSON] ()
  , _acceptedMembers :: route :-
    "api" :> "meetup"
          :> Capture "meetup-id" MeetupId
          :> "accepted-members.json"
          :> Get '[JSON] [AT.PublicAccountInfo]
  , _userEventCreate :: route :- AuthProtect "cookie-auth" :>
    "api" :> "user" :> "events"
          :> "create.json"
          :> ReqBody '[ JSON] AT.EventUpdateForm
          :> Post '[JSON] AT.Event
  , _userEventUpdate :: route :- AuthProtect "cookie-auth" :>
    "api" :> "user" :> "events"
          :> Capture "event-id" EventId
          :> "update.json"
          :> ReqBody '[ JSON] AT.EventUpdateForm
          :> Post '[JSON] AT.Event
  } deriving (Generic)

server :: API (AsServerT AppM)
server =
  API
    { __index = indexEndpoint
    , __login = indexEndpoint
    , __meetups = indexEndpoint
    , __meetup = \_ -> indexEndpoint
    , __meetupApplicationForm = \_ -> indexEndpoint
    , __account = indexEndpoint
    , __accountMeetups = indexEndpoint
    , __accountMeetupInfo = \_ -> indexEndpoint
    , __accountMeetupEvents = \_ -> indexEndpoint
    , __accountMeetupCreate = indexEndpoint
    , __accountMeetupEdit = \_ -> indexEndpoint
    , __accountEventsCreate = \_ -> indexEndpoint
    , __accountEventsEdit = \_ _ -> indexEndpoint
    , __ping = pongEndpoint
    , __jsonApi = toServant jsonApi
    , __fbLoginCallback = fbLoginCallbackEndpoint
    , __googleLoginCallback = googleLoginCallback
    }
  where
    jsonApi :: JsonAPI (AsServerT AppM)
    jsonApi =
      JsonAPI
        { _meetupsList = meetupsList
        , _meetupInfo = meetupInfo
        , _meetupAuthInfo = meetupAuthInfo
        , _eventsList = eventsList
        , _eventInfo = eventInfo
        , _accountInfo = accountInfoEndpoint
        , _publicAccountInfo = publicAccountInfo
        , _logInSendPassword = logInSendPasswordEndpoint
        , _logInSendCode = logInSendCodeEndpoint
        , _logOut = logOut
        , _updateAccount = updateAccount
        , _attendedMeetups = attendedMeetups
        , _meetupFeedbackForms = meetupFeedbackForms
        , _feedbackFormInfo = feedbackFormInfo
        , _feedbackGet = feedbackGet
        , _feedbackSet = feedbackSet
        , _rsvpList = rsvpList
        , _rsvpCreate = rsvpCreate
        , _rsvpDelete = rsvpDelete
        , _applicationForm = applicationForm
        , _applicationFieldSave = applicationFieldSave
        , _applicationFormSubmit = applicationFormSubmit
        , _applicationFormDelete = applicationFormDelete
        , _userApplicationField = userApplicationField
        , _userMeetups = userMeetups
        , _userMeetupInfo = userMeetupInfo
        , _userMeetupCreate = userMeetupCreate
        , _userMeetupUpdate = userMeetupUpdate
        , _userMeetupApplications = userMeetupApplications
        , _userMeetupApplicationAccept = userMeetupApplicationAccept
        , _userMeetupApplicationReject = userMeetupApplicationReject
        , _acceptedMembers = acceptedMembers
        , _userEventCreate = userEventCreate
        , _userEventUpdate = userEventUpdate
        }
