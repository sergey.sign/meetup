module Le.Components exposing (..)

import Bootstrap.Alert as Alert
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Navbar as Navbar
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Le.Api as Api
import Le.Block.Toast
import Le.Util exposing (..)


isInvalidCls fld formErrors =
    case Dict.get fld formErrors of
        Just e ->
            classList [ ( "is-invalid", True ) ]

        Nothing ->
            classList [ ( "is-invalid", False ) ]


fieldError fld formErrors =
    case Dict.get fld formErrors of
        Just e ->
            [ div [ class "invalid-feedback" ] [ text e ] ]

        Nothing ->
            []


formGenField formErrors lbl errFld inpHtml =
    div [ class "form-group" ]
        [ label [ class "form-label-w100" ] <|
            [ div [ class "div-label-for" ]
                [ text lbl
                ]
            , inpHtml
            ]
                ++ fieldError errFld formErrors
        ]


formTextField formErrors lbl val errFld oninp =
    formGenField formErrors lbl errFld <|
        input
            [ class "form-control"
            , placeholder lbl
            , type_ "text"
            , value val
            , onInput oninp
            ]
            []


titleSpinnerInfosGeneric toToastMsg vp title model content optnavbar =
    [ Le.Block.Toast.view toToastMsg vp.now model.toasts <|
        (div [] <|
            [ div []
                [ div [ class "global-spinner" ]
                    [ div
                        [ class "spinner-border"
                        , classList [ ( "d-none", model.loading <= 0 ) ]
                        , attribute "role" "status"
                        ]
                        [ span [ class "sr-only" ] [ text "Loading..." ]
                        ]
                    ]
                , div [ class "page-title" ] [ title ]
                ]
            ]
                ++ optnavbar
                ++ content
        )
    ]


titleSpinnerInfos toToastMsg vp title model content =
    titleSpinnerInfosGeneric toToastMsg vp title model content []


loadingSpinner =
    div
        [ class "spinner-border text-primary"
        , attribute "role" "status"
        ]
        [ span [ class "sr-only" ] [ text "Loading..." ]
        ]


catalogCardGrid items =
    let
        -- renderDeck : List (Card.Config ) -> Html Msg
        renderDeck xs =
            Grid.row
                []
                (List.map
                    (Grid.col
                        [ Col.xs12
                        , Col.sm12
                        , Col.md6
                        , Col.lg4
                        , Col.xl4
                        , Col.attrs [ class "mb-2" ]
                        ]
                        << List.singleton
                        << Card.view
                    )
                    xs
                )

        renderItem item =
            Card.config [ Card.attrs [] ]
                |> Card.block [ Block.attrs [ class "shadow border-0" ] ]
                    [ Block.titleH2 [] [ item.title ]
                    , Block.text []
                        [ div [] [ item.content ]
                        ]
                    ]
    in
    Grid.container []
        [ renderDeck
            (List.map renderItem items)
        ]


locationBlock event =
    div [] <|
        [ text "Location: "
        ]
            ++ (case event.location of
                    Nothing ->
                        [ text "Not Specified" ]

                    Just l ->
                        [ text l
                        ]
                            ++ (case event.location_link of
                                    Nothing ->
                                        []

                                    Just link ->
                                        [ text " ", a [ href link ] [ text "(map)" ] ]
                               )
               )


rsvpInfoBlock eventRsvps event =
    let
        rsvps =
            fromMaybe [] (Dict.get event.id eventRsvps)

        renderRsvp rsvp =
            li [] [ text rsvp.name ]

        usersNum =
            List.length rsvps
    in
    if usersNum == 0 then
        text "Nobody RSVPd"

    else
        div []
            [ text <| "Users who RSVPd (" ++ String.fromInt usersNum ++ ")"
            , ul []
                (List.map renderRsvp rsvps)
            ]
