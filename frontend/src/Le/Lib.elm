module Le.Lib exposing (..)

import Dict exposing (Dict)
import Http
import Json.Encode as Json
import Le.Api as Api
import Le.Block.Toast
import Le.Ports as Ports


type alias ModelWithErrorFields model =
    { model
        | formErrors : Dict String String
        , toasts : Le.Block.Toast.Model
        , loading : Int
    }


handleHttpError : (Le.Block.Toast.Msg -> msg) -> Api.Error -> ModelWithErrorFields model -> ( ModelWithErrorFields model, Cmd msg )
handleHttpError toMsg e m =
    case e.httpError of
        Http.BadStatus code ->
            case code of
                401 ->
                    ( { m | loading = m.loading - 1 }, Ports.needLoginRedirect () )

                403 ->
                    ( { m | loading = m.loading - 1 }, Ports.needLoginRedirect () )

                _ ->
                    ( { m
                        | formErrors = e.formErrors
                        , loading = m.loading - 1
                      }
                    , Le.Block.Toast.addErrors toMsg e.errors
                    )

        _ ->
            ( { m | loading = m.loading - 1 }
            , Le.Block.Toast.addError toMsg "There was an error, please try again later"
            )


handleHttpErrorNoRedirect : (Le.Block.Toast.Msg -> msg) -> Api.Error -> ModelWithErrorFields model -> ( ModelWithErrorFields model, Cmd msg )
handleHttpErrorNoRedirect toMsg e m =
    case e.httpError of
        Http.BadStatus code ->
            case code of
                401 ->
                    ( { m | loading = m.loading - 1 }, Cmd.none )

                403 ->
                    ( { m | loading = m.loading - 1 }, Cmd.none )

                _ ->
                    ( { m
                        | formErrors = e.formErrors
                        , loading = m.loading - 1
                      }
                    , Le.Block.Toast.addErrors toMsg e.errors
                    )

        _ ->
            ( { m | loading = m.loading - 1 }
            , Le.Block.Toast.addError toMsg "There was an error, please try again later"
            )


handleHttpSuccess : ModelWithErrorFields model -> ModelWithErrorFields model
handleHttpSuccess m =
    let
        loading2 =
            if m.loading < 0 then
                0

            else
                m.loading - 1
    in
    { m | loading = loading2 }


updateMsgs :
    (msg -> model -> ( model, Cmd msg ))
    -> List msg
    -> model
    -> ( model, Cmd msg )
updateMsgs update msgs model =
    case msgs of
        [] ->
            ( model, Cmd.none )

        x :: xs ->
            let
                ( model2, cmd ) =
                    update x model

                ( model3, cmd2 ) =
                    updateMsgs update xs model2
            in
            ( model3, Cmd.batch [ cmd, cmd2 ] )


handleSubpage model m pageModel setPageModel pageUpdate toMsg page =
    let
        ( newPageModel, pageCmd ) =
            pageUpdate m pageModel

        newModel =
            setPageModel newPageModel

        newCmd =
            Cmd.map toMsg pageCmd
    in
    ( newModel, newCmd )


fromFormErrors : Dict String String -> String -> Result String a -> Result String a
fromFormErrors formErrors field val =
    case Dict.get field formErrors of
        Nothing ->
            val

        Just err ->
            Err err


incLoading : ModelWithErrorFields model -> ModelWithErrorFields model
incLoading model =
    { model | loading = model.loading + 1 }


incLoadingBy : Int -> ModelWithErrorFields model -> ModelWithErrorFields model
incLoadingBy i model =
    { model | loading = model.loading + i }
