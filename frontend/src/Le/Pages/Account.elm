module Le.Pages.Account exposing
    ( Model
    , Msg(..)
    , init
    , loadAccountInfo
    , update
    , view
    )

import Bootstrap.Alert as Alert
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Navbar as Navbar
import Browser.Navigation as Navigation
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Encode as Json
import Le.Api as Api
import Le.Block.Toast
import Le.Components exposing (..)
import Le.Lib exposing (..)
import Le.Pages.AccountCommon
import Le.Ports as Ports
import Le.Routes
import Le.Types exposing (..)
import Le.Util exposing (..)
import Task
import Time exposing (Month(..), Weekday(..))


type Msg
    = NoOp
    | ToastMsg Le.Block.Toast.Msg
    | GotAccountInfo (Result Api.Error Api.AccountInfo)
    | LogOutPressed
    | GotMeetupsAttended (Result Api.Error (List Api.AccountAttendedMeetup))
    | GotFeedbackForms Api.MeetupId (Result Api.Error (List Api.FeedbackForm))
    | NavbarMsg Navbar.State


type alias Model =
    { toasts : Le.Block.Toast.Model
    , formErrors : Dict String String
    , loading : Int
    , accInfo : Maybe Api.AccountInfo
    , attended : List Api.AccountAttendedMeetup
    , feedbackForms : Dict Api.MeetupId (List Api.FeedbackForm)
    , navbar : Navbar.State
    }


init : ( Model, Cmd Msg )
init =
    let
        ( navbar, navbarCmd ) =
            Navbar.initialState NavbarMsg
    in
    ( { toasts = Le.Block.Toast.init
      , formErrors = Dict.empty
      , loading = 0
      , accInfo = Nothing
      , attended = []
      , feedbackForms = Dict.empty
      , navbar = navbar
      }
    , Cmd.batch
        [ loadAccountInfo
        , Api.getApiMeetupsAttendedjson GotMeetupsAttended
        , navbarCmd
        ]
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ToastMsg imsg ->
            let
                ( im, icmds ) =
                    Le.Block.Toast.update imsg model.toasts
            in
            ( { model | toasts = im }, Cmd.map ToastMsg icmds )

        NavbarMsg state ->
            ( { model | navbar = state }, Cmd.none )

        LogOutPressed ->
            ( model, Navigation.load Le.Routes.logOut )

        GotAccountInfo (Err e) ->
            handleHttpError ToastMsg e model

        GotAccountInfo (Ok accInfo) ->
            if accInfo.info_complete then
                ( handleHttpSuccess { model | accInfo = Just accInfo }
                , Cmd.none
                )

            else
                ( handleHttpSuccess model
                , Ports.needLoginRedirect ()
                )

        GotMeetupsAttended (Err e) ->
            handleHttpError ToastMsg e model

        GotMeetupsAttended (Ok attended) ->
            ( { model | attended = attended }
            , Cmd.batch (List.map (\x -> Api.getApiMeetupsByMeetupidFeedbackformsjson x.meetup.id (GotFeedbackForms x.meetup.id)) attended)
            )

        GotFeedbackForms meetupId (Err e) ->
            handleHttpError ToastMsg e model

        GotFeedbackForms meetupId (Ok forms) ->
            let
                feedbackForms2 =
                    Dict.insert meetupId forms model.feedbackForms
            in
            ( { model | feedbackForms = feedbackForms2 }, Cmd.none )


loadAccountInfo =
    Api.getApiAccountinfojson GotAccountInfo


renderNeedLogin : Html Msg
renderNeedLogin =
    text "Loading..."


renderAccountInfo : Api.AccountInfo -> Html Msg
renderAccountInfo accInfo =
    div []
        [ text <| "Hello, " ++ accInfo.email
        ]


view : ViewParams -> Model -> List (Html Msg)
view vp model =
    let
        renderAttendedMeetups =
            [ h2 [ class "mt-4" ] [ text "Attended Meetups" ]
            ]
                ++ (case model.attended of
                        [] ->
                            [ text "No meetups yet" ]

                        _ ->
                            List.map renderAttended model.attended
                   )

        renderAttendedDate event =
            li [ class "list-item" ]
                [ text (renderDateStr event.date) ]

        renderFeedbackFormLink form =
            li [ class "list-group-item" ]
                [ a [ href <| Le.Routes.feedbackForm form.id ]
                    [ text form.title ]
                ]

        feedbackFormsBlock attended =
            case Dict.get attended.meetup.id model.feedbackForms of
                Nothing ->
                    []

                Just [] ->
                    []

                Just forms ->
                    [ Block.titleH4 [] [ text "Available feedback forms" ]
                    , Block.text [] <|
                        [ ul [ class "list-group" ]
                            (List.map renderFeedbackFormLink forms)
                        ]
                    ]

        renderAttended : Api.AccountAttendedMeetup -> Html Msg
        renderAttended attended =
            Card.view
                (Card.block []
                    ([ Block.titleH4 []
                        [ a [ href <| Le.Routes.meetup attended.meetup.id ]
                            [ text attended.meetup.title ]
                        ]
                     , Block.text []
                        [ case attended.complete of
                            False ->
                                text "Meetup is still ongoing."

                            True ->
                                text "Meetup is complete."
                        ]
                     ]
                        ++ feedbackFormsBlock attended
                        ++ [ Block.text []
                                [ p []
                                    [ p [] [ text "Dates you attended:" ]
                                    , ul [ class "list-group" ]
                                        (List.map renderAttendedDate attended.events)
                                    ]
                                ]
                           ]
                    )
                    (Card.config [ Card.attrs [ class "mb-4" ] ])
                )

        nav =
            Navbar.config NavbarMsg
                |> Navbar.items
                    [ Navbar.itemLink [ href Le.Routes.account ] [ text "General" ]
                    , Navbar.itemLink [ href Le.Routes.accountMeetups ] [ text "Your Meetups" ]
                    , Navbar.itemLink
                        [ class "link-like"
                        , onClick LogOutPressed
                        ]
                        [ text "Log Out" ]
                    ]
                |> Navbar.view model.navbar
    in
    titleSpinnerInfos ToastMsg vp (h1 [ class "display-4" ] [ text "Account Info" ]) model <|
        [ Le.Pages.AccountCommon.nav NavbarMsg LogOutPressed model.navbar
        , maybe renderNeedLogin renderAccountInfo model.accInfo
        ]
            ++ renderAttendedMeetups
