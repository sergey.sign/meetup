module Le.Pages.MeetupEdit exposing (..)

import Bootstrap.Alert as Alert
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Navbar as Navbar
import Browser.Navigation as Navigation
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Encode as Json
import Le.Api as Api
import Le.Block.Toast
import Le.Components exposing (..)
import Le.Components.Markdown
import Le.Components.Select
import Le.Constants exposing (..)
import Le.Lib exposing (..)
import Le.Pages.AccountCommon
import Le.Ports as Ports
import Le.Routes
import Le.Types exposing (..)
import Le.Util exposing (..)
import Markdown
import Task
import Time exposing (Month(..), Weekday(..))


type Msg
    = NoOp
    | FormChange Form
    | ToastMsg Le.Block.Toast.Msg
    | GotAccountInfo (Result Api.Error Api.AccountInfo)
    | LogOutPressed
    | NavbarMsg Navbar.State
    | GotMeetup (Result Api.Error Api.UserMeetupInfo)
    | SaveClicked
    | SaveDone (Result Api.Error Api.UserMeetupInfo)


type alias Form =
    { title : String
    , description : String
    , member_approval_required : Bool
    , time_zone : String
    }


type alias Model =
    { toasts : Le.Block.Toast.Model
    , formErrors : Dict String String
    , loading : Int
    , mMeetupId : Maybe Api.MeetupId
    , accInfo : Maybe Api.AccountInfo
    , navbar : Navbar.State
    , form : Form
    , meetup : Maybe Api.UserMeetupInfo
    }


init : Maybe Api.MeetupId -> ( Model, Cmd Msg )
init mMeetupId =
    let
        ( navbar, navbarCmd ) =
            Navbar.initialState NavbarMsg
    in
    ( { toasts = Le.Block.Toast.init
      , formErrors = Dict.empty
      , loading = 0
      , mMeetupId = mMeetupId
      , accInfo = Nothing
      , navbar = navbar
      , form =
            { title = ""
            , description = ""
            , member_approval_required = False
            , time_zone = ""
            }
      , meetup = Nothing
      }
    , Cmd.batch
        [ Api.getApiAccountinfojson GotAccountInfo
        , navbarCmd
        , case mMeetupId of
            Nothing ->
                Cmd.none

            Just meetupId ->
                Api.getApiUserMeetupsByMeetupidInfojson meetupId GotMeetup
        ]
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        form =
            model.form
    in
    case msg of
        NoOp ->
            ( model, Cmd.none )

        FormChange f ->
            ( { model | form = f }, Cmd.none )

        ToastMsg imsg ->
            let
                ( im, icmds ) =
                    Le.Block.Toast.update imsg model.toasts
            in
            ( { model | toasts = im }, Cmd.map ToastMsg icmds )

        NavbarMsg state ->
            ( { model | navbar = state }, Cmd.none )

        LogOutPressed ->
            ( model, Navigation.load Le.Routes.logOut )

        GotAccountInfo (Err e) ->
            handleHttpError ToastMsg e model

        GotAccountInfo (Ok accInfo) ->
            if accInfo.info_complete then
                ( handleHttpSuccess { model | accInfo = Just accInfo }
                , Cmd.none
                )

            else
                ( handleHttpSuccess model
                , Ports.needLoginRedirect ()
                )

        GotMeetup (Err e) ->
            handleHttpError ToastMsg e model

        GotMeetup (Ok meetup) ->
            ( { model
                | meetup = Just meetup
                , form =
                    { form
                        | title = meetup.title
                        , description = meetup.description
                        , member_approval_required = meetup.member_approval_required
                        , time_zone = meetup.time_zone
                    }
              }
            , Cmd.none
            )

        SaveClicked ->
            let
                frm : Api.MeetupUpdateForm
                frm =
                    { title = form.title
                    , description = form.description
                    , member_approval_required = form.member_approval_required
                    , time_zone = form.time_zone
                    }
            in
            ( model
            , case model.mMeetupId of
                Nothing ->
                    Api.postApiUserMeetupsCreatejson frm SaveDone

                Just meetupId ->
                    Api.postApiUserMeetupsByMeetupidUpdatejson meetupId frm SaveDone
            )

        SaveDone (Err e) ->
            handleHttpError ToastMsg e model

        SaveDone (Ok meetup) ->
            let
                text =
                    case model.mMeetupId of
                        Nothing ->
                            "Meetup created successfully"

                        Just _ ->
                            "Meetup updated successfully"
            in
            ( { model
                | meetup = Just meetup
                , mMeetupId = Just meetup.id
              }
            , Cmd.batch
                [ Le.Block.Toast.addInfo ToastMsg text
                , Api.getApiUserMeetupsByMeetupidInfojson meetup.id GotMeetup
                ]
            )


view : ViewParams -> Model -> List (Html Msg)
view vp model =
    let
        form =
            model.form

        renderItem item =
            { title = a [ href <| Le.Routes.accountMeetupInfo item.id ] [ text item.title ]
            , content = div [] []
            }

        genField =
            formGenField model.formErrors

        textField =
            formTextField model.formErrors

        renderMeetup =
            div [ class "page-meetup-edit" ]
                [ h2 [ class "mb-2" ] <|
                    [ div []
                        [ input
                            [ type_ "text"
                            , value form.title
                            , onInput (\x -> FormChange { form | title = x })
                            , class "form-control form-control-lg"
                            ]
                            []
                        ]
                    ]
                        ++ fieldError "title" model.formErrors
                , div [ class "mb-2" ]
                    [ genField "Description" "description" <|
                        Le.Components.Markdown.editor
                            form.description
                            (\v -> FormChange { form | description = v })
                    ]
                , div [ class "mb-4" ] <|
                    [ div [ class "form-check" ]
                        [ input
                            [ class "form-check-input"
                            , type_ "checkbox"
                            , checked form.member_approval_required
                            , onClick (FormChange { form | member_approval_required = not form.member_approval_required })
                            ]
                            []
                        , label [ class "form-check-label align-midle ml-2" ]
                            [ text "Member approval required" ]
                        ]
                    ]
                        ++ fieldError "member_approval_required" model.formErrors
                , div [ class "mb-4 " ] <|
                    [ label []
                        [ text "Time Zone" ]
                    , div []
                        [ Le.Components.Select.view
                            (Just form.time_zone)
                            [ class "custom-select page-meetup-edit--input-max-width"
                            , onInput <| \x -> FormChange { form | time_zone = x }
                            ]
                            timezones
                        ]
                    ]
                        ++ fieldError "time_zone" model.formErrors
                , div [ class "text-center" ]
                    [ button
                        [ class "btn btn-primary btn-lg"
                        , onClick SaveClicked
                        ]
                        [ text "Save" ]
                    ]
                ]

        title =
            case model.mMeetupId of
                Nothing ->
                    "Create Meetup"

                Just _ ->
                    "Edit Meetup"
    in
    titleSpinnerInfos ToastMsg vp (h1 [ class "display-4" ] [ text title ]) model <|
        [ Le.Pages.AccountCommon.nav NavbarMsg LogOutPressed model.navbar
        , case model.mMeetupId of
            Nothing ->
                renderMeetup

            Just meetupId ->
                case model.meetup of
                    Nothing ->
                        loadingSpinner

                    Just meetup ->
                        renderMeetup
        ]
