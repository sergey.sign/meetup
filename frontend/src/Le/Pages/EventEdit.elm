module Le.Pages.EventEdit exposing (..)

import Bootstrap.Alert as Alert
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Navbar as Navbar
import Browser.Navigation as Navigation
import Calendar
import Clock
import DateTime
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Encode as Json
import Le.Api as Api
import Le.Block.Toast
import Le.Components exposing (..)
import Le.Components.Markdown
import Le.Components.Select
import Le.Constants exposing (..)
import Le.Lib exposing (..)
import Le.Pages.AccountCommon
import Le.Ports as Ports
import Le.Routes
import Le.Types exposing (..)
import Le.Util exposing (..)
import Markdown
import Task
import Time exposing (Month(..), Weekday(..))


type Msg
    = NoOp
    | FormChange Form
    | ToastMsg Le.Block.Toast.Msg
    | GotAccountInfo (Result Api.Error Api.AccountInfo)
    | LogOutPressed
    | NavbarMsg Navbar.State
    | GotEvent (Result Api.Error Api.Event)
    | SaveClicked
    | SaveDone (Result Api.Error Api.Event)
    | GotTime Time.Posix


type alias Form =
    { date : Api.IntZonedTime
    , location : String
    , location_link : String
    }


type alias Model =
    { toasts : Le.Block.Toast.Model
    , formErrors : Dict String String
    , loading : Int
    , meetupId : Api.MeetupId
    , mEventId : Maybe Api.EventId
    , accInfo : Maybe Api.AccountInfo
    , navbar : Navbar.State
    , form : Form
    , event : Maybe Api.Event
    }


init : Api.MeetupId -> Maybe Api.EventId -> ( Model, Cmd Msg )
init meetupId mEventId =
    let
        ( navbar, navbarCmd ) =
            Navbar.initialState NavbarMsg
    in
    ( { toasts = Le.Block.Toast.init
      , formErrors = Dict.empty
      , loading = 0
      , meetupId = meetupId
      , mEventId = mEventId
      , accInfo = Nothing
      , navbar = navbar
      , form =
            { date = 0
            , location = ""
            , location_link = ""
            }
      , event = Nothing
      }
    , Cmd.batch
        [ Api.getApiAccountinfojson GotAccountInfo
        , navbarCmd
        , Task.perform GotTime Time.now
        , case mEventId of
            Nothing ->
                Cmd.none

            Just eventId ->
                Api.getApiEventsByEventidInfojson eventId GotEvent
        ]
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        form =
            model.form
    in
    case msg of
        NoOp ->
            ( model, Cmd.none )

        FormChange f ->
            ( { model | form = f }, Cmd.none )

        ToastMsg imsg ->
            let
                ( im, icmds ) =
                    Le.Block.Toast.update imsg model.toasts
            in
            ( { model | toasts = im }, Cmd.map ToastMsg icmds )

        NavbarMsg state ->
            ( { model | navbar = state }, Cmd.none )

        LogOutPressed ->
            ( model, Navigation.load Le.Routes.logOut )

        GotTime t ->
            ( { model | form = { form | date = Time.posixToMillis t } }, Cmd.none )

        GotAccountInfo (Err e) ->
            handleHttpError ToastMsg e model

        GotAccountInfo (Ok accInfo) ->
            if accInfo.info_complete then
                ( handleHttpSuccess { model | accInfo = Just accInfo }
                , Cmd.none
                )

            else
                ( handleHttpSuccess model
                , Ports.needLoginRedirect ()
                )

        GotEvent (Err e) ->
            handleHttpError ToastMsg e model

        GotEvent (Ok event) ->
            ( { model
                | event = Just event
                , form =
                    { form
                        | date = event.date
                        , location = fromMaybe "" event.location
                        , location_link = fromMaybe "" event.location_link
                    }
              }
            , Cmd.none
            )

        SaveClicked ->
            let
                frm : Api.EventUpdateForm
                frm =
                    { date = form.date
                    , location = form.location
                    , location_link = form.location_link
                    , meetup_id = model.meetupId
                    }
            in
            ( model
            , case model.mEventId of
                Nothing ->
                    Api.postApiUserEventsCreatejson frm SaveDone

                Just eventId ->
                    Api.postApiUserEventsByEventidUpdatejson eventId frm SaveDone
            )

        SaveDone (Err e) ->
            handleHttpError ToastMsg e model

        SaveDone (Ok event) ->
            let
                text =
                    case model.mEventId of
                        Nothing ->
                            "Event created successfully"

                        Just _ ->
                            "Event updated successfully"
            in
            ( { model
                | event = Just event
                , mEventId = Just event.id
              }
            , Cmd.batch
                [ Le.Block.Toast.addInfo ToastMsg text
                , Api.getApiEventsByEventidInfojson event.id GotEvent
                ]
            )


view : ViewParams -> Model -> List (Html Msg)
view vp model =
    let
        form =
            model.form

        renderItem item =
            { title = a [ href <| Le.Routes.accountMeetupInfo item.id ] [ text item.title ]
            , content = div [] []
            }

        genField =
            formGenField model.formErrors

        textField =
            formTextField model.formErrors

        renderEvent =
            div [ class "page-event-edit" ]
                [ div [ class "mb-2" ] <|
                    [ genField "Date" "date" <|
                        input
                            [ type_ "date"
                            , value <| renderDateValue (Time.millisToPosix form.date)
                            , onInput
                                (\x ->
                                    case parseDateValue x (Time.millisToPosix form.date) of
                                        Nothing ->
                                            NoOp

                                        Just d ->
                                            FormChange { form | date = Time.posixToMillis d }
                                )
                            , class "form-control"
                            ]
                            []
                    ]
                , div [ class "mb-2" ] <|
                    [ genField "Time" "time" <|
                        input
                            [ type_ "time"
                            , value <| renderTimeValue (Time.millisToPosix form.date)
                            , onInput
                                (\x ->
                                    case parseTimeValue x (Time.millisToPosix form.date) of
                                        Nothing ->
                                            NoOp

                                        Just d ->
                                            FormChange { form | date = Time.posixToMillis d }
                                )
                            , class "form-control"
                            ]
                            []
                    ]
                , div [ class "mb-2" ]
                    [ textField "Location" form.location "location" (\x -> FormChange { form | location = x })
                    ]
                , div [ class "mb-2" ]
                    [ textField "Location link" form.location_link "location_link" (\x -> FormChange { form | location_link = x })
                    ]
                , div [ class "text-center" ]
                    [ button
                        [ class "btn btn-primary btn-lg"
                        , onClick SaveClicked
                        ]
                        [ text "Save" ]
                    ]
                ]

        title =
            case model.mEventId of
                Nothing ->
                    "Create Event"

                Just _ ->
                    "Edit Event"
    in
    titleSpinnerInfos ToastMsg vp (h1 [ class "display-4" ] [ text title ]) model <|
        [ Le.Pages.AccountCommon.nav NavbarMsg LogOutPressed model.navbar
        , case model.mEventId of
            Nothing ->
                renderEvent

            Just meetupId ->
                case model.event of
                    Nothing ->
                        loadingSpinner

                    Just meetup ->
                        renderEvent
        ]
