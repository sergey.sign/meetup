module Le.Pages.ApplicationForm exposing (..)

import Bootstrap.Alert as Alert
import Bootstrap.Button as Button
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as J
import Json.Encode as Json
import Le.Api as Api
import Le.Block.Toast
import Le.Components exposing (..)
import Le.Components.Markdown
import Le.Lib exposing (..)
import Le.Routes
import Le.Types exposing (..)
import Le.Util exposing (..)
import Markdown
import Maybe.Extra
import Task
import Time exposing (Month(..), Weekday(..))


type Msg
    = NoOp
    | ToastMsg Le.Block.Toast.Msg
    | FormChanged Model
    | MeetupLoaded (Result Api.Error Api.Meetup)
    | TimeLoaded Time.Posix
    | AccountLoaded (Result Api.Error Api.AccountInfo)
    | GotMeetupAuthInfo (Result Api.Error Api.MeetupAuthInfo)
    | GotApplicationForm (Result Api.Error Api.ApplicationForm)
    | SubmitPressed
    | FieldUpdateDone Api.ApplicationFieldId (Result Api.Error ())
    | FormSubmitDone (Result Api.Error ())
    | GotUserApplicationField Api.ApplicationFieldId (Result Api.Error (Maybe Api.UserApplicationField))


type alias Model =
    { toasts : Le.Block.Toast.Model
    , formErrors : Dict String String
    , loading : Int
    , meetupId : Int
    , now : Maybe Time.Posix
    , meetup : Maybe Api.Meetup
    , meetupAuthInfo : Maybe Api.MeetupAuthInfo
    , applicationForm : Maybe Api.ApplicationForm
    , events : List Api.Event
    , account : Maybe Api.AccountInfo
    , eventRsvps : Dict Int (List Api.RsvpInfo)
    , fieldVals : Dict Api.ApplicationFieldId String
    , fieldsAwaitingSuccess : List Api.ApplicationFieldId
    , fieldsAwaitingLoaded : List Api.ApplicationFieldId
    , formSubmitDone : Bool
    }


init : Int -> ( Model, Cmd Msg )
init meetupId =
    ( { toasts = Le.Block.Toast.init
      , formErrors = Dict.empty
      , loading = 0
      , meetupId = meetupId
      , now = Nothing
      , meetup = Nothing
      , meetupAuthInfo = Nothing
      , applicationForm = Nothing
      , events = []
      , account = Nothing
      , eventRsvps = Dict.fromList []
      , fieldVals = Dict.empty
      , fieldsAwaitingSuccess = []
      , fieldsAwaitingLoaded = []
      , formSubmitDone = False
      }
    , Cmd.batch
        [ Api.getApiMeetupByMeetupidInfojson meetupId MeetupLoaded
        , Api.getApiMeetupByMeetupidAuthinfojson meetupId GotMeetupAuthInfo
        , nowCmd
        , Api.getApiAccountinfojson AccountLoaded
        , Api.getApiMeetupByMeetupidApplicationformjson meetupId GotApplicationForm
        ]
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ToastMsg imsg ->
            let
                ( im, icmds ) =
                    Le.Block.Toast.update imsg model.toasts
            in
            ( { model | toasts = im }, Cmd.map ToastMsg icmds )

        FormChanged m2 ->
            ( m2, Cmd.none )

        MeetupLoaded (Err e) ->
            handleHttpError ToastMsg e model

        MeetupLoaded (Ok meetup) ->
            ( { model | meetup = Just meetup }
            , Cmd.none
            )

        TimeLoaded now ->
            ( { model | now = Just now }, Cmd.none )

        AccountLoaded (Err e) ->
            handleHttpError ToastMsg e model

        AccountLoaded (Ok acc) ->
            ( { model | account = Just acc }, Cmd.none )

        GotMeetupAuthInfo (Err e) ->
            handleHttpError ToastMsg e model

        GotMeetupAuthInfo (Ok meetupAuthInfo) ->
            ( { model | meetupAuthInfo = Just meetupAuthInfo }
            , Cmd.none
            )

        GotApplicationForm (Err e) ->
            handleHttpError ToastMsg e model

        GotApplicationForm (Ok applicationForm) ->
            ( { model
                | applicationForm = Just applicationForm
                , fieldsAwaitingLoaded = List.map .id applicationForm.fields
              }
            , Cmd.batch
                (List.map
                    (\x -> Api.getApiUserapplicationfieldByUserapplicationfieldidInfojson x.id (GotUserApplicationField x.id))
                    applicationForm.fields
                )
            )

        SubmitPressed ->
            case model.applicationForm of
                Nothing ->
                    ( model, Cmd.none )

                Just applicationForm ->
                    let
                        cmds =
                            List.map
                                (\( fieldId, content ) ->
                                    Api.postApiApplicationfieldByApplicationfieldidUpdatejson fieldId { content = content } (FieldUpdateDone fieldId)
                                )
                                (Dict.toList model.fieldVals)
                    in
                    ( { model | fieldsAwaitingSuccess = List.map .id applicationForm.fields }
                    , Cmd.batch cmds
                    )

        FieldUpdateDone fieldId (Err e) ->
            handleHttpError ToastMsg e model

        FieldUpdateDone fieldId (Ok ()) ->
            case model.applicationForm of
                Nothing ->
                    ( model, Cmd.none )

                Just applicationForm ->
                    let
                        stillAwaiting =
                            List.filter
                                (\x -> x /= fieldId)
                                model.fieldsAwaitingSuccess

                        cmd =
                            if List.length stillAwaiting > 0 then
                                Cmd.none

                            else
                                Api.postApiApplicationformByApplicationformidSubmitjson applicationForm.id FormSubmitDone
                    in
                    ( { model | fieldsAwaitingSuccess = stillAwaiting }
                    , cmd
                    )

        FormSubmitDone (Err e) ->
            handleHttpError ToastMsg e model

        FormSubmitDone (Ok ()) ->
            ( { model | formSubmitDone = True }, Cmd.none )

        GotUserApplicationField fieldId (Err e) ->
            handleHttpError ToastMsg e model

        GotUserApplicationField fieldId (Ok muserApplicationField) ->
            let
                m2 =
                    case muserApplicationField of
                        Just userApplicationField ->
                            { model
                                | fieldVals =
                                    Dict.insert
                                        userApplicationField.field_id
                                        userApplicationField.content
                                        model.fieldVals
                            }

                        Nothing ->
                            model
            in
            ( { m2 | fieldsAwaitingLoaded = List.filter (\x -> x /= fieldId) model.fieldsAwaitingLoaded }
            , Cmd.none
            )


nowCmd : Cmd Msg
nowCmd =
    Task.perform TimeLoaded Time.now


mdEditor attrs children =
    node "markdown-editor" attrs children


view : ViewParams -> Model -> List (Html Msg)
view vp model =
    let
        title =
            "Application Form"

        rsvpInfoBlock event =
            let
                rsvps =
                    fromMaybe [] (Dict.get event.id model.eventRsvps)

                renderRsvp rsvp =
                    li [] [ text rsvp.name ]

                usersNum =
                    List.length rsvps
            in
            if usersNum == 0 then
                text "Nobody RSVPd"

            else
                div []
                    [ text <| "Users who RSVPd (" ++ String.fromInt usersNum ++ ")"
                    , ul []
                        (List.map renderRsvp rsvps)
                    ]

        locationBlock event =
            div [] <|
                [ text "Location: "
                ]
                    ++ (case event.location of
                            Nothing ->
                                [ text "Not Specified" ]

                            Just l ->
                                [ text l
                                ]
                                    ++ (case event.location_link of
                                            Nothing ->
                                                []

                                            Just link ->
                                                [ text " ", a [ href link ] [ text "(map)" ] ]
                                       )
                       )

        renderMeetupForm meetup meetupAuthInfo applicationForm =
            div [] <|
                [ div [] <| Markdown.toHtml Nothing applicationForm.description
                ]
                    ++ List.map renderField applicationForm.fields
                    ++ [ div [ class "text-center" ]
                            [ button
                                [ class "btn btn-primary btn-lg mb-4"
                                , onClick SubmitPressed
                                ]
                                [ text "Submit" ]
                            ]
                       ]

        genField =
            formGenField model.formErrors

        renderField field =
            if List.length model.fieldsAwaitingLoaded > 0 then
                loadingSpinner

            else
                div []
                    [ div [] (Markdown.toHtml Nothing field.description)
                    , genField "" ("content-" ++ String.fromInt field.id) <|
                        Le.Components.Markdown.editor
                            (fromMaybe "" (Dict.get field.id model.fieldVals))
                            (\v -> FormChanged { model | fieldVals = Dict.insert field.id v model.fieldVals })
                    ]

        submitDone =
            text "Thank you! Your form has been submitted successfully. Meetup organizers will contact you once there is a result."

        inner =
            if model.formSubmitDone then
                submitDone

            else
                case ( model.meetup, model.meetupAuthInfo, model.applicationForm ) of
                    ( Just meetup, Just meetupAuthInfo, Just applicationForm ) ->
                        renderMeetupForm meetup meetupAuthInfo applicationForm

                    _ ->
                        loadingSpinner

        -- Markdown.toHtml Nothing meetup.description
    in
    titleSpinnerInfos ToastMsg vp (h1 [ class "display-4" ] [ text title ]) model <|
        [ inner ]
