module Le.Pages.Login exposing
    ( Model
    , Msg(..)
    , init
    , loadAccountInfo
    , update
    , view
    )

import Bootstrap.Alert as Alert
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as J
import Le.Api as Api
import Le.Block.Toast
import Le.Components exposing (..)
import Le.Lib exposing (..)
import Le.Types exposing (..)
import Le.Util exposing (..)


type Msg
    = NoOp
    | ToastMsg Le.Block.Toast.Msg
    | EmailKeyDown Int
    | CodeKeyDown Int
    | GotAccountInfo (Result Api.Error Api.AccountInfo)
    | EnterEmail
    | EmailChange String
    | CodeChange String
    | EnterCode
    | EmailSent (Result Api.Error ())
    | GotLoginResponse (Result Api.Error Api.AccountInfo)
    | FullNameChange String
    | PhoneNumberChange String
    | SaveProfileDetails
    | UpdateAccountDone (Result Api.Error Api.AccountInfo)


type alias Model =
    { toasts : Le.Block.Toast.Model
    , formErrors : Dict String String
    , loading : Int
    , email : String
    , code : String
    , oneTimePassword : String
    , showCodeInput : Bool
    , accInfo : Maybe Api.AccountInfo
    , fullName : String
    , phoneNumber : String
    }


init : Model
init =
    { toasts = Le.Block.Toast.init
    , formErrors = Dict.fromList []
    , loading = 0
    , email = ""
    , code = ""
    , oneTimePassword = ""
    , showCodeInput = False
    , accInfo = Nothing
    , fullName = ""
    , phoneNumber = ""
    }


fillAccInfo : Api.AccountInfo -> Model -> Model
fillAccInfo accInfo model =
    { model
        | accInfo = Just accInfo
        , phoneNumber = fromMaybe "" accInfo.phone_number
        , fullName = fromMaybe "" accInfo.full_name
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        emailEnterPressed =
            ( incLoading model
            , Api.postApiLoginsendpassword { email = model.email } EmailSent
            )

        codeEnterPressed =
            ( incLoading model
            , Api.postApiLogin
                { email = model.email
                , code = model.code
                }
                GotLoginResponse
            )
    in
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ToastMsg imsg ->
            let
                ( im, icmds ) =
                    Le.Block.Toast.update imsg model.toasts
            in
            ( { model | toasts = im }, Cmd.map ToastMsg icmds )

        EmailKeyDown key ->
            if key == 13 then
                emailEnterPressed

            else
                ( model, Cmd.none )

        CodeKeyDown key ->
            if key == 13 then
                codeEnterPressed

            else
                ( model, Cmd.none )

        EnterEmail ->
            emailEnterPressed

        EmailChange v ->
            ( { model | email = v }, Cmd.none )

        CodeChange v ->
            ( { model | code = v }, Cmd.none )

        EnterCode ->
            codeEnterPressed

        EmailSent (Err e) ->
            ( { model | loading = model.loading - 1 }
            , Le.Block.Toast.addError ToastMsg "Error while sending one-time code, please try again"
            )

        EmailSent (Ok ()) ->
            ( handleHttpSuccess { model | showCodeInput = True }
            , Le.Block.Toast.addInfo ToastMsg "Email sent successfully. Please check your inbox"
            )

        GotLoginResponse (Err e) ->
            ( { model | loading = model.loading - 1 }
            , Le.Block.Toast.addError ToastMsg "Error while trying to log in"
            )

        GotLoginResponse (Ok accInfo) ->
            let
                m2 =
                    fillAccInfo accInfo model
            in
            ( handleHttpSuccess { m2 | showCodeInput = False }
            , Cmd.none
            )

        GotAccountInfo (Err e) ->
            handleHttpErrorNoRedirect ToastMsg e model

        GotAccountInfo (Ok accInfo) ->
            ( handleHttpSuccess { model | accInfo = Just accInfo }
            , Cmd.none
            )

        FullNameChange v ->
            ( { model | fullName = v }, Cmd.none )

        PhoneNumberChange v ->
            ( { model | phoneNumber = v }, Cmd.none )

        SaveProfileDetails ->
            ( incLoading model
            , Api.postApiAccountUpdatejson
                { full_name = model.fullName
                , phone_number = model.phoneNumber
                }
                UpdateAccountDone
            )

        UpdateAccountDone (Err e) ->
            handleHttpErrorNoRedirect ToastMsg e model

        UpdateAccountDone (Ok accInfo) ->
            -- handled by parent update
            ( handleHttpSuccess { model | accInfo = Just accInfo }
            , Cmd.none
            )


loadAccountInfo =
    Api.getApiAccountinfojson GotAccountInfo


loginForm model =
    let
        codeInput =
            [ div [ class "mb-1" ] <|
                [ label [] <|
                    [ div [ class "div-label-for" ]
                        [ text "Code" ]
                    , input
                        [ type_ "text"
                        , class "form-control"
                        , isInvalidCls "phone_number" model.formErrors
                        , placeholder "Enter code"
                        , value model.code
                        , onInput CodeChange
                        , on "keydown" (J.map CodeKeyDown keyCode)
                        ]
                        []
                    ]
                        ++ fieldError "login" model.formErrors
                , span
                    [ class "btn btn-light"
                    , onClick EnterCode
                    ]
                    [ text "Enter code" ]
                ]
            ]
    in
    Html.form [ onSubmit NoOp ] <|
        [ div [ class "form-group" ] <|
            [ div [ class "mb-1" ] <|
                [ label [] <|
                    [ div [ class "div-label-for" ]
                        [ text "Email address" ]
                    , input
                        [ type_ "email"
                        , class "form-control"
                        , isInvalidCls "email" model.formErrors
                        , placeholder "Enter email"
                        , value model.email
                        , onInput EmailChange
                        , attribute "autocomplete" "email"
                        , on "keydown" (J.map EmailKeyDown keyCode)
                        ]
                        []
                    ]
                ]
                    ++ fieldError "email" model.formErrors
                    ++ [ span
                            [ class "btn btn-light"
                            , onClick EnterEmail
                            ]
                            [ text "Send password" ]
                       ]
            ]
                ++ (if model.showCodeInput then
                        codeInput

                    else
                        []
                   )
        , div []
            [ a [ href "https://www.facebook.com/v3.2/dialog/oauth?client_id=492171828232643&redirect_uri=https%3A%2F%2Fmeetup.events%2Fapi%2Ffb-login-callback&state=csrf&scope=email,public_profile" ]
                [ text "Sign in via Facebook" ]
            ]
        , div []
            [ a [ href "https://accounts.google.com/o/oauth2/v2/auth?scope=email%20profile&response_type=code&state=csrf&redirect_uri=https%3A%2F%2Fmeetup.events%2Fapi%2Fgoogle-login-callback&client_id=364430847946-d8kr2la1k524vhksvd7qb8pee0qm0ibr.apps.googleusercontent.com" ]
                [ text "Sign in via Google" ]
            ]
        ]


fillAccountForm vp model accInfo =
    let
        form =
            [ div [ class "mb-1" ] <|
                [ label [] <|
                    [ div [ class "div-label-for" ]
                        [ text "Full Name" ]
                    , input
                        [ type_ "text"
                        , class "form-control"
                        , isInvalidCls "full_name" model.formErrors
                        , placeholder "Enter full name"
                        , value model.fullName
                        , onInput FullNameChange
                        ]
                        []
                    ]
                        ++ fieldError "full_name" model.formErrors
                ]
            , div [ class "mb-1" ] <|
                [ label [] <|
                    [ div [ class "div-label-for" ]
                        [ text "Phone Number" ]
                    , input
                        [ type_ "text"
                        , class "form-control"
                        , isInvalidCls "phone_number" model.formErrors
                        , placeholder "Enter phone number"
                        , value model.phoneNumber
                        , onInput PhoneNumberChange
                        ]
                        []
                    ]
                        ++ fieldError "phone_number" model.formErrors
                ]
            , span
                [ class "btn btn-light"
                , onClick SaveProfileDetails
                ]
                [ text "Save" ]
            ]

        title =
            "Complete Account Details"
    in
    titleSpinnerInfos ToastMsg vp (h1 [ class "display-4" ] [ text title ]) model <|
        form


view : ViewParams -> Model -> List (Html Msg)
view vp model =
    let
        signInView =
            titleSpinnerInfos ToastMsg vp (h1 [ class "display-4" ] [ text "Sign In" ]) model <|
                [ loginForm model ]
    in
    [ div [ class "container" ] <|
        case model.accInfo of
            Nothing ->
                signInView

            Just accInfo ->
                fillAccountForm vp model accInfo
    ]
