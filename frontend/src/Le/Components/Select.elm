module Le.Components.Select exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


view : Maybe String -> List (Attribute msg) -> List ( String, String ) -> Html msg
view sel attrs xs =
    let
        viewOpt ( v, label ) =
            let
                optAttrs =
                    [ value v ]
                        ++ (if Just v == sel then
                                [ attribute "selected" "1" ]

                            else
                                []
                           )
            in
            option optAttrs [ text label ]
    in
    select attrs
        (List.map viewOpt xs)
