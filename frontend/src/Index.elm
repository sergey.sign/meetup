module Main exposing (main)

import Bootstrap.Alert as Alert
import Bootstrap.Button as Button
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.ListGroup as Listgroup
import Bootstrap.Modal as Modal
import Bootstrap.Navbar as Navbar
import Bootstrap.Text as Text
import Browser exposing (UrlRequest)
import Browser.Dom
import Browser.Events
import Browser.Navigation as Navigation
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Http
import Json.Encode as Json
import Le.Api as Api
import Le.Lib exposing (handleSubpage, updateMsgs)
import Le.Pages.Account as AccountPage
import Le.Pages.AccountMeetupInfo as AccountMeetupInfoPage
import Le.Pages.AccountMeetups as AccountMeetupsPage
import Le.Pages.ApplicationForm as ApplicationFormPage
import Le.Pages.Catalog as CatalogPage
import Le.Pages.EventEdit as EventEditPage
import Le.Pages.FeedbackForm as FeedbackFormPage
import Le.Pages.Home as HomePage
import Le.Pages.Login as LoginPage
import Le.Pages.Meetup as MeetupPage
import Le.Pages.MeetupEdit as MeetupEditPage
import Le.Pages.MeetupEvents as MeetupEventsPage
import Le.Ports as Ports
import Le.Routes as Routes
import Le.Types exposing (..)
import Le.Util exposing (..)
import Markdown
import String
import Task
import Time
import Url exposing (Url)
import Url.Builder as UrlBuilder
import Url.Parser as UrlParser exposing ((</>), Parser, int, s, top)


type alias Flags =
    {}


type Msg
    = UrlChange Url
    | ClickedLink UrlRequest
    | OnResize Int Int
    | GotViewport Browser.Dom.Viewport
    | GotTime Time.Posix
    | NavMsg Navbar.State
    | CatalogPageMsg CatalogPage.Msg
    | MeetupPageMsg MeetupPage.Msg
    | ApplicationFormPageMsg ApplicationFormPage.Msg
    | FeedbackFormPageMsg FeedbackFormPage.Msg
    | LoginPageMsg LoginPage.Msg
    | AccountPageMsg AccountPage.Msg
    | AccountMeetupsPageMsg AccountMeetupsPage.Msg
    | AccountMeetupInfoPageMsg AccountMeetupInfoPage.Msg
    | MeetupEventsPageMsg MeetupEventsPage.Msg
    | MeetupEditPageMsg MeetupEditPage.Msg
    | EventEditPageMsg EventEditPage.Msg


type alias Model =
    { navKey : Navigation.Key
    , page : Page
    , navState : Navbar.State
    , viewport : Maybe Browser.Dom.Viewport
    , now : Maybe Time.Posix
    , pageModel : PageModel
    , redirectOnLogin : Maybe Page
    }


type Page
    = PageHome
    | PageCatalog
    | PageMeetup Int
    | PageApplicationForm Int
    | PageFeedbackForm Int
    | PageLogin
    | PageAccount
    | PageAccountMeetups
    | PageAccountMeetupInfo Api.MeetupId
    | PageMeetupEvents Api.MeetupId
    | PageMeetupEdit Api.MeetupId
    | PageMeetupCreate
    | PageEventCreate Api.MeetupId
    | PageEventEdit Api.MeetupId Api.EventId
    | PageNotFound


type PageModel
    = HomePageModel ()
    | CatalogPageModel CatalogPage.Model
    | MeetupPageModel MeetupPage.Model
    | ApplicationFormPageModel ApplicationFormPage.Model
    | FeedbackFormPageModel FeedbackFormPage.Model
    | LoginPageModel LoginPage.Model
    | AccountPageModel AccountPage.Model
    | AccountMeetupsPageModel AccountMeetupsPage.Model
    | AccountMeetupInfoPageModel AccountMeetupInfoPage.Model
    | MeetupEventsPageModel MeetupEventsPage.Model
    | MeetupEditPageModel MeetupEditPage.Model
    | EventEditPageModel EventEditPage.Model


initPageModel : Page -> ( PageModel, Cmd Msg )
initPageModel p =
    case p of
        PageHome ->
            ( HomePageModel (), Cmd.none )

        PageCatalog ->
            let
                ( m, cmds ) =
                    CatalogPage.init
            in
            ( CatalogPageModel m, Cmd.map CatalogPageMsg cmds )

        PageMeetup i ->
            let
                ( m, cmds ) =
                    MeetupPage.init i
            in
            ( MeetupPageModel m, Cmd.map MeetupPageMsg cmds )

        PageApplicationForm i ->
            let
                ( m, cmds ) =
                    ApplicationFormPage.init i
            in
            ( ApplicationFormPageModel m, Cmd.map ApplicationFormPageMsg cmds )

        PageFeedbackForm i ->
            let
                ( m, cmds ) =
                    FeedbackFormPage.init i
            in
            ( FeedbackFormPageModel m, Cmd.map FeedbackFormPageMsg cmds )

        PageLogin ->
            ( LoginPageModel LoginPage.init, Cmd.none )

        PageAccount ->
            let
                ( m, cmds ) =
                    AccountPage.init
            in
            ( AccountPageModel m, Cmd.map AccountPageMsg cmds )

        PageAccountMeetups ->
            let
                ( m, cmds ) =
                    AccountMeetupsPage.init
            in
            ( AccountMeetupsPageModel m, Cmd.map AccountMeetupsPageMsg cmds )

        PageAccountMeetupInfo i ->
            let
                ( m, cmds ) =
                    AccountMeetupInfoPage.init i
            in
            ( AccountMeetupInfoPageModel m, Cmd.map AccountMeetupInfoPageMsg cmds )

        PageMeetupEvents i ->
            let
                ( m, cmds ) =
                    MeetupEventsPage.init i
            in
            ( MeetupEventsPageModel m, Cmd.map MeetupEventsPageMsg cmds )

        PageMeetupEdit i ->
            let
                ( m, cmds ) =
                    MeetupEditPage.init (Just i)
            in
            ( MeetupEditPageModel m, Cmd.map MeetupEditPageMsg cmds )

        PageEventEdit mi i ->
            let
                ( m, cmds ) =
                    EventEditPage.init mi (Just i)
            in
            ( EventEditPageModel m, Cmd.map EventEditPageMsg cmds )

        PageMeetupCreate ->
            let
                ( m, cmds ) =
                    MeetupEditPage.init Nothing
            in
            ( MeetupEditPageModel m, Cmd.map MeetupEditPageMsg cmds )

        PageEventCreate i ->
            let
                ( m, cmds ) =
                    EventEditPage.init i Nothing
            in
            ( EventEditPageModel m, Cmd.map EventEditPageMsg cmds )

        PageNotFound ->
            ( HomePageModel (), Cmd.none )


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = ClickedLink
        , onUrlChange = UrlChange
        }


init : Flags -> Url -> Navigation.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        ( navState, navCmd ) =
            Navbar.initialState NavMsg

        initModel : Model
        initModel =
            { navKey = key
            , navState = navState
            , viewport = Nothing
            , now = Nothing
            , page = PageHome
            , pageModel = HomePageModel ()
            , redirectOnLogin = Nothing
            }

        ( model, urlCmd ) =
            urlUpdate initModel url
    in
    ( model
    , Cmd.batch
        [ urlCmd
        , navCmd
        , Task.perform GotTime Time.now
        ]
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Browser.Events.onResize OnResize
        , Time.every 5000 GotTime
        , Navbar.subscriptions model.navState NavMsg
        ]


needLogin model backPage =
    ( { model
        | page = PageLogin
        , pageModel = LoginPageModel LoginPage.init
        , redirectOnLogin = Just backPage
      }
    , Cmd.map LoginPageMsg <| LoginPage.loadAccountInfo
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        setPm c pm =
            { model | pageModel = c pm }
    in
    case ( model.pageModel, msg ) of
        ( _, ClickedLink req ) ->
            case req of
                Browser.Internal url ->
                    ( model
                    , Navigation.pushUrl model.navKey <| Url.toString url
                    )

                Browser.External href ->
                    ( model, Navigation.load href )

        ( _, UrlChange url ) ->
            urlUpdate model url

        ( _, GotTime t ) ->
            ( { model | now = Just t }, Cmd.none )

        ( _, NavMsg state ) ->
            ( { model | navState = state }
            , Cmd.none
            )

        ( _, OnResize _ _ ) ->
            ( model
            , Task.perform GotViewport Browser.Dom.getViewport
            )

        ( _, GotViewport vp ) ->
            ( { model | viewport = Just vp }
            , Cmd.none
            )

        ( CatalogPageModel pageModel, CatalogPageMsg m ) ->
            handleSubpage
                update
                m
                pageModel
                (\pm -> { model | pageModel = CatalogPageModel pm })
                CatalogPage.update
                CatalogPageMsg
                PageCatalog

        ( MeetupPageModel pageModel, MeetupPageMsg m ) ->
            handleSubpage
                update
                m
                pageModel
                (\pm -> { model | pageModel = MeetupPageModel pm })
                MeetupPage.update
                MeetupPageMsg
                PageMeetup

        ( ApplicationFormPageModel pageModel, ApplicationFormPageMsg m ) ->
            handleSubpage
                update
                m
                pageModel
                (\pm -> { model | pageModel = ApplicationFormPageModel pm })
                ApplicationFormPage.update
                ApplicationFormPageMsg
                PageApplicationForm

        ( FeedbackFormPageModel pageModel, FeedbackFormPageMsg m ) ->
            handleSubpage
                update
                m
                pageModel
                (\pm -> { model | pageModel = FeedbackFormPageModel pm })
                FeedbackFormPage.update
                FeedbackFormPageMsg
                PageFeedbackForm

        ( LoginPageModel pageModel, LoginPageMsg ((LoginPage.GotLoginResponse (Ok accInfo)) as m) ) ->
            if accInfo.info_complete then
                ( model, Ports.redirectBackAfterLogin () )

            else
                handleSubpage
                    model
                    m
                    pageModel
                    (setPm LoginPageModel)
                    LoginPage.update
                    LoginPageMsg
                    PageLogin

        ( _, LoginPageMsg (LoginPage.UpdateAccountDone (Ok _)) ) ->
            redirectBack model

        ( LoginPageModel pageModel, LoginPageMsg m ) ->
            let
                ( newPageModel, pageMsgs ) =
                    LoginPage.update m pageModel
            in
            ( { model | pageModel = LoginPageModel newPageModel }
            , Cmd.map LoginPageMsg pageMsgs
            )

        ( AccountPageModel pageModel, AccountPageMsg m ) ->
            handleSubpage
                update
                m
                pageModel
                (\pm -> { model | pageModel = AccountPageModel pm })
                AccountPage.update
                AccountPageMsg
                PageAccount

        ( AccountMeetupsPageModel pageModel, AccountMeetupsPageMsg m ) ->
            handleSubpage
                update
                m
                pageModel
                (\pm -> { model | pageModel = AccountMeetupsPageModel pm })
                AccountMeetupsPage.update
                AccountMeetupsPageMsg
                PageAccountMeetups

        ( AccountMeetupInfoPageModel pageModel, AccountMeetupInfoPageMsg m ) ->
            handleSubpage
                update
                m
                pageModel
                (\pm -> { model | pageModel = AccountMeetupInfoPageModel pm })
                AccountMeetupInfoPage.update
                AccountMeetupInfoPageMsg
                PageAccountMeetupInfo

        ( MeetupEventsPageModel pageModel, MeetupEventsPageMsg m ) ->
            handleSubpage
                update
                m
                pageModel
                (\pm -> { model | pageModel = MeetupEventsPageModel pm })
                MeetupEventsPage.update
                MeetupEventsPageMsg
                PageMeetupEvents

        ( MeetupEditPageModel pageModel, MeetupEditPageMsg m ) ->
            handleSubpage
                update
                m
                pageModel
                (\pm -> { model | pageModel = MeetupEditPageModel pm })
                MeetupEditPage.update
                MeetupEditPageMsg
                PageMeetupEdit

        ( EventEditPageModel pageModel, EventEditPageMsg m ) ->
            handleSubpage
                update
                m
                pageModel
                (\pm -> { model | pageModel = EventEditPageModel pm })
                EventEditPage.update
                EventEditPageMsg
                PageEventEdit

        _ ->
            ( model, Cmd.none )


urlUpdate : Model -> Url -> ( Model, Cmd Msg )
urlUpdate model url =
    case UrlParser.parse routeParser url of
        Just route ->
            let
                ( pm, cmds ) =
                    initPageModel route
            in
            ( { model | page = route, pageModel = pm }, cmds )

        Nothing ->
            ( { model
                | page = PageNotFound
              }
            , Cmd.none
            )


decode : Url -> Maybe Page
decode url =
    { url | path = Maybe.withDefault "" url.fragment, fragment = Nothing }
        |> UrlParser.parse routeParser


redirectBack model =
    let
        p =
            case model.redirectOnLogin of
                Just page ->
                    page

                Nothing ->
                    PageAccount

        ( m, cmds ) =
            initPageModel p
    in
    ( { model | page = p, pageModel = m, redirectOnLogin = Nothing }, cmds )


routeParser : Parser (Page -> a) a
routeParser =
    UrlParser.oneOf
        [ UrlParser.map PageHome top
        , UrlParser.map PageCatalog (s "meetups")
        , UrlParser.map PageMeetup (s "meetup" </> int)
        , UrlParser.map PageApplicationForm (s "meetup" </> int </> s "application-form")
        , UrlParser.map PageFeedbackForm (s "feedback-form" </> int)
        , UrlParser.map PageAccount (s "account")
        , UrlParser.map PageAccountMeetups (s "account" </> s "meetups")
        , UrlParser.map PageAccountMeetupInfo (s "account" </> s "meetups" </> int </> s "info")
        , UrlParser.map PageMeetupEvents (s "account" </> s "meetups" </> int </> s "events")
        , UrlParser.map PageMeetupCreate (s "account" </> s "meetups" </> s "create")
        , UrlParser.map PageEventCreate (s "account" </> s "meetups" </> int </> s "events" </> s "create")
        , UrlParser.map PageMeetupEdit (s "account" </> s "meetups" </> int </> s "edit")
        , UrlParser.map PageEventEdit (s "account" </> s "meetups" </> int </> s "events" </> int </> s "edit")
        , UrlParser.map PageLogin (s "login")
        ]


view : Model -> Browser.Document Msg
view model =
    { title = "Meetup.Events"
    , body =
        [ div []
            [ menu model
            , mainContent model
            ]
        ]
    }


menu : Model -> Html Msg
menu model =
    Navbar.config NavMsg
        |> Navbar.withAnimation
        |> Navbar.brand [ href "/" ] [ text "MEETUP.EVENTS" ]
        |> Navbar.items
            [ Navbar.itemLink [ href "/" ] [ text "Main" ]
            , Navbar.itemLink [ href Routes.meetups ] [ text "Meetups" ]
            , Navbar.itemLink [ href Routes.account ] [ text "Account" ]
            ]
        |> Navbar.view model.navState


mainContent : Model -> Html Msg
mainContent model =
    let
        vp : ViewParams
        vp =
            { isMobile = isMobile model.viewport
            , now = model.now
            }
    in
    Grid.container [] <|
        case ( model.page, model.pageModel ) of
            ( PageHome, HomePageModel _ ) ->
                HomePage.view

            ( PageCatalog, CatalogPageModel pm ) ->
                (List.map << Html.map) CatalogPageMsg <| CatalogPage.view vp pm

            ( PageMeetup _, MeetupPageModel pm ) ->
                (List.map << Html.map) MeetupPageMsg <| MeetupPage.view vp pm

            ( PageApplicationForm _, ApplicationFormPageModel pm ) ->
                (List.map << Html.map) ApplicationFormPageMsg <| ApplicationFormPage.view vp pm

            ( PageFeedbackForm _, FeedbackFormPageModel pm ) ->
                (List.map << Html.map) FeedbackFormPageMsg <| FeedbackFormPage.view vp pm

            ( PageAccount, AccountPageModel pm ) ->
                (List.map << Html.map) AccountPageMsg <| AccountPage.view vp pm

            ( PageAccountMeetups, AccountMeetupsPageModel pm ) ->
                (List.map << Html.map) AccountMeetupsPageMsg <| AccountMeetupsPage.view vp pm

            ( PageAccountMeetupInfo i, AccountMeetupInfoPageModel pm ) ->
                (List.map << Html.map) AccountMeetupInfoPageMsg <| AccountMeetupInfoPage.view vp pm

            ( PageMeetupEvents i, MeetupEventsPageModel pm ) ->
                (List.map << Html.map) MeetupEventsPageMsg <| MeetupEventsPage.view vp pm

            ( PageMeetupEdit i, MeetupEditPageModel pm ) ->
                (List.map << Html.map) MeetupEditPageMsg <| MeetupEditPage.view vp pm

            ( PageEventEdit mi i, EventEditPageModel pm ) ->
                (List.map << Html.map) EventEditPageMsg <| EventEditPage.view vp pm

            ( PageMeetupCreate, MeetupEditPageModel pm ) ->
                (List.map << Html.map) MeetupEditPageMsg <| MeetupEditPage.view vp pm

            ( PageEventCreate m, EventEditPageModel pm ) ->
                (List.map << Html.map) EventEditPageMsg <| EventEditPage.view vp pm

            ( PageLogin, LoginPageModel pm ) ->
                (List.map << Html.map) LoginPageMsg <| LoginPage.view vp pm

            ( PageNotFound, _ ) ->
                pageNotFound

            _ ->
                pageNotFound


pageNotFound : List (Html Msg)
pageNotFound =
    [ h1 [] [ text "Not found" ]
    , text "Sorry couldn't find that page"
    ]
